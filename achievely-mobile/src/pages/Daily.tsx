import React from 'react';
import { useHistory } from "react-router-dom";

import { IonContent, IonHeader,  IonIcon, IonPage, IonTitle, IonToolbar, IonButtons, IonMenuButton, IonSearchbar, IonFab, IonFabList, IonFabButton } from '@ionic/react';

import ContentHeader from '../components/ContentHeader';
import DailyList from '../features/daily/DailyList'
import { addOutline, receiptOutline, rocketOutline } from 'ionicons/icons';
import './Daily.css';

/**
 * Action Button
 */
const AddFab: React.FC = () => {

  const history = useHistory();

  const openForm = (type: string) => {
    history.push(`/new/${type}`);
  };

  return(
    <>
      <IonFab slot="fixed" vertical="bottom" horizontal="center">
        <IonFabButton data-qa="add-tab-btn" >
          <IonIcon icon={addOutline} />
        </IonFabButton>
        <IonFabList side="top">
          <IonFabButton color="primary" onClick={() => openForm('noema')} data-qa="add-noema-tab-btn">
            <IonIcon icon={receiptOutline} />
          </IonFabButton>
          <IonFabButton color="secondary" onClick={() => openForm('task')} data-qa="add-task-tab-btn">
            <IonIcon icon={rocketOutline} />
          </IonFabButton>
        </IonFabList>
      </IonFab>
    </>
  )
};

const Daily: React.FC = () => {

  return (
    <IonPage>
      <IonHeader translucent={true}>
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
          <IonTitle>Daily</IonTitle>
          { false && <IonSearchbar showCancelButton="always" placeholder="Search" ></IonSearchbar>}
          
        </IonToolbar>
      </IonHeader>

      <IonContent fullscreen className="card-background-page">
        <ContentHeader />
        <DailyList />
        
      </IonContent>
      <AddFab />
    </IonPage>
  );
};

export default Daily;
