import React, { useEffect } from 'react';

import { useParams, useHistory, useLocation } from "react-router-dom";

import { IonContent, IonHeader, IonBackButton, IonToggle, IonPage, IonTitle, IonToolbar, IonButtons, IonMenuButton, IonLabel, IonSearchbar } from '@ionic/react';

import { useSelector } from 'react-redux';
import { RootState } from '../store/rootReducer';

import NoemaForm from '../features/noema/NoemaForm';
import TaskForm from '../features/noema/TaskForm'
// import { leafOutline } from 'ionicons/icons';
import './Daily.css';
import AppHeader from '../components/AppHeader';

interface ParamType {
  type: string,
  uid: string
}

function useQuery() {
  return new URLSearchParams(useLocation().search);
}

const EntryForm: React.FC = () => {

  const history = useHistory();
  const routeParams = useParams<ParamType>();
  const query = useQuery();

  const noemas = useSelector(
    (state: RootState) => state.noemas
  );

  const entry = routeParams.uid ? noemas.byIds[routeParams.uid] : undefined;
  const backHref =  query.get('backHref');

  const [isTask, setIsTask] = React.useState(routeParams.type === 'task');

  // TODO 
  useEffect(() => {
    setIsTask(routeParams.type === 'task');
  }, [setIsTask])

  return (
    <IonPage>
      <AppHeader title={isTask ? "Task" : "Journal"} navBackHref={backHref} />

      <IonContent fullscreen className="card-background-page">
        {!isTask ? <NoemaForm entry={entry} onComplete={() => { history.push("/tabs/daily"); }} />
          : <TaskForm entry={entry} onComplete={() => { history.push("/tabs/daily"); }} />}
      </IonContent>
    </IonPage>
  );
};

export default EntryForm;
