import React from 'react';

import { useHistory } from "react-router-dom";

import { 
  IonContent,
  IonPage, 
} from '@ionic/react';

import AppHeader from '../components/AppHeader';

import NoemaForm from '../features/noema/NoemaForm';
import NoemaList from '../features/noema/NoemaList'
import './Journal.css';

/**
 * Journey page.
 * Displays list of Noemas only (no Tasks)
 */
const Tab1: React.FC = () => {

  const history = useHistory();

  return (
    <IonPage>
      <AppHeader title="Journal" />

      <IonContent fullscreen className="card-background-page">

        <NoemaForm onComplete={ () => { history.push("/journal"); } } />
        <hr />
        <NoemaList />
      </IonContent>
    </IonPage>
  );
};

export default Tab1;
