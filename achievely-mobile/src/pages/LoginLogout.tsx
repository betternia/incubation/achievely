import React from 'react';
import { useSelector } from 'react-redux';
import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';

import { RootState } from '../store/rootReducer';
import AppHeader from '../components/AppHeader';
import './Home.scss';
import LoginButtons from '../features/session/LoginButtons';

const LoginLogout: React.FC = () => {
  const session = useSelector(
    (state: RootState) => state.session
  );
  
  return (
    <IonPage>
      <AppHeader title="Login" />

      <IonContent fullscreen>
        {/* <ContentHeader /> */}
          <IonToolbar>
            <LoginButtons session={session}/>
          </IonToolbar>
      </IonContent>
    </IonPage>
  );
};

export default LoginLogout;
