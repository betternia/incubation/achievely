import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams, useHistory } from "react-router-dom";

import { IonContent, IonHeader, IonIcon, IonPage, IonTitle, IonToolbar, IonButtons, IonButton, IonBackButton, IonFab, IonFabButton } from '@ionic/react';
import { createOutline, star, chevronBack, chevronForward } from 'ionicons/icons';

import { RootState } from '../store/rootReducer';

import NoemaDetails from '../features/noema/NoemaDetails';
import TaskDetails from '../features/noema/TaskDetails';
import { toggleHighlight } from '../features/noema/noema.slice'

interface ParamType {
  uid: string
}

// export default function EntryDetails({ entry }:EntryDetailsProps ) {
const EntryDetails: React.FC = () => {

  const dispatch = useDispatch();

  const history = useHistory();
  const routeParams = useParams<ParamType>();

  const noemas = useSelector(
    (state: RootState) => state.noemas
  );

  const entry = noemas.byIds[routeParams.uid];
  // console.log('route:' + routeParams.uid, 'entry:' + JSON.stringify(entry));

  return (
    <IonPage>
      <IonHeader translucent={true}>
        <IonToolbar>
          <IonButtons slot="start">
            <IonBackButton defaultHref="/tabs/daily" />
          </IonButtons>
          <IonButtons slot="end">
            <IonButton onClick={() => { dispatch(toggleHighlight(entry.uid!)) }}>
              <IonIcon color={entry.highlighted ? "warning" : "dark"} slot="icon-only" icon={star}></IonIcon>
            </IonButton>
            <IonButton onClick={() => history.push(`/edit/${entry.uid}?backHref=/entry/${entry.uid}`)}>
              <IonIcon slot="icon-only" icon={createOutline}></IonIcon>
            </IonButton>
          </IonButtons>
          <IonTitle>Entry</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent fullscreen className="card-background-page">

        {(entry.kind === 'noema') && <NoemaDetails entry={entry} />}
        {(entry.kind === 'task') && <TaskDetails entry={entry} />}

      </IonContent>

      <IonFab slot="fixed" vertical="bottom" horizontal="start">
        <IonFabButton color="light">
          <IonIcon icon={chevronBack} />
        </IonFabButton>
      </IonFab>
      <IonFab slot="fixed" vertical="bottom" horizontal="end">
        <IonFabButton color="light">
          <IonIcon icon={chevronForward} />
        </IonFabButton>
      </IonFab>
    </IonPage>
  );
};

export default EntryDetails;
