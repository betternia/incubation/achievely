import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux'

import { useIonViewWillEnter, IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonButtons, IonButton, IonMenuButton, IonCard, IonCardHeader, IonCardSubtitle, IonCardTitle, IonCardContent } from '@ionic/react';

import { RootState } from '../store/rootReducer';
import { fetchUserAccount } from '../features/session/user.service';
import { AuthService } from '../utils/auth.service';

import LoginButtons from '../features/session/LoginButtons';
import './Landing.scss';
import AppHeader from '../components/AppHeader';

const Landing: React.FC = () => {
  const dispatch = useDispatch();

  const session = useSelector(
    (state: RootState) => state.session
  );

  // Using useEffect it gets called twice at the logging flow, even before LoginRedirect handles the flow completion
  // useEffect(() => {
  useIonViewWillEnter(() => {
    console.debug('Landing:useIonViewWillEnter session: ', session);
    if (session.account === undefined) {
      (async () => {
        // NOTE: when loadTokenFromStorage() is called from App, it interferes with login flow by firing event
        // and causing Login listener to fail.
        // await AuthService.Instance.loadTokenFromStorage();
        // console.log('Landing isAuthenticated: ' + (AuthService.Instance.session.isAuthenticated ? 'true': 'false') );
        // if (AuthService.Instance.session.isAuthenticated) {
        //   const account = await fetchUserAccount();
        //   AuthService.startSession(dispatch, account);
        // }
      })();
    }
  });

  return (
    <IonPage>
      <AppHeader title="Achievely" />

      <IonContent fullscreen>

        <div className="landing-background">
          {/* <img src="/assets/pexels-johannes-plenio-1632790.jpg" alt="Hero" /> */}
          <IonButtons>
            <LoginButtons session={session}/>
          </IonButtons>
        </div>

        <IonCard>
          <IonCardHeader>
            <IonCardSubtitle>Achieve your goals, introspect your life</IonCardSubtitle>
            <IonCardTitle>And Plant Trees!</IonCardTitle>
          </IonCardHeader>

          <IonCardContent>
            <IonButton href="/tabs/daily" >To Journal</IonButton>
            We are soo glad to see you.
            Are you ready to save the planet!!
            YEAH!
          </IonCardContent>
        </IonCard>

      </IonContent>
    </IonPage>
  );
};

export default Landing;
