import React from 'react';
import { useSelector } from 'react-redux';
import { IonContent, IonHeader, IonPage, IonText, IonTitle, IonToolbar } from '@ionic/react';

import { RootState } from '../store/rootReducer';
import AppHeader from '../components/AppHeader';
import './Home.scss';
import LoginButtons from '../features/session/LoginButtons';

const About: React.FC = () => {
  const session = useSelector(
    (state: RootState) => state.session
  );
  
  return (
    <IonPage>
      <AppHeader title="Login" />

      <IonContent fullscreen>
          <IonText color='primary'><h2>Achievely v.0.2(auth0)</h2></IonText>
          <p>This is Achievely under heavy development</p>
      </IonContent>
    </IonPage>
  );
};

export default About;
