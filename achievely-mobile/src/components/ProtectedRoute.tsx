
/* /src/routes/privateRoute.jsx */
import React from "react";
import { useSelector } from 'react-redux';
import { Route, Redirect } from "react-router-dom";

import { RootState } from '../store/rootReducer';
import { AuthService } from "../utils/auth.service";

export const ProtectedRoute = ({ component, ...rest }: any) => {

    const session = useSelector(
        (state: RootState) => state.session
    );

    function renderFn(props: any) {
        console.log('ProtectedRoute isAuthenticated: ' + (AuthService.Instance.session.isAuthenticated ? 'true' : 'false'));
        console.log('ProtectedRoute accountUid: ' + session.account?.uid);
        if (session.account || AuthService.Instance.session.isAuthenticated) {
            // if (true) {
            return React.createElement(component, props);
        }
        return <Redirect to="/landing" />
    }

    return <Route {...rest} render={renderFn} />;
};
