import React from 'react';

import { IonHeader, IonToolbar, IonButtons, IonMenuButton, IonBackButton, IonTitle } from '@ionic/react';

interface AppHeaderProps {
  title?: string,
  navBackHref?: string | null
}

const AppHeader: React.FC<AppHeaderProps> = ({ title, navBackHref }) => {

  return (
    <IonHeader translucent={true}>
      <IonToolbar>
        {navBackHref ? (<IonButtons slot="start">
          <IonBackButton defaultHref={navBackHref} />
        </IonButtons>) : (
        <IonButtons slot="start">
          <IonMenuButton />
        </IonButtons>
        )
        }

        <IonTitle>{title}</IonTitle>

      </IonToolbar>
    </IonHeader>
  );

}

export default AppHeader;
