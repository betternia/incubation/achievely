import React from 'react';
import { useSelector } from 'react-redux';

import { IonHeader, IonIcon, IonText, IonCardHeader, IonCardSubtitle, IonCardTitle } from '@ionic/react';
import { leafOutline } from 'ionicons/icons';

import { RootState } from '../store/rootReducer';


export default function ContentHeader() {

  const session = useSelector(
    (state: RootState) => state.session
  );
  const user = session.account?.user;

  const username = session && (user?.alternateName || user?.givenName);
  const pictureUrl = session && (user?.image || "https://www.gravatar.com/avatar?d=mm&s=50");

  return (
    <IonHeader color="light" className="ion-no-border" style={{ background: "url(/assets/pexels-kate-trifo-3804997.jpg) no-repeat top/cover fixed" }}>
      <IonCardHeader>
        <div style={{ display: "flex" }}>

          <img src={pictureUrl} alt="avatar" style={{ height: 50, borderRadius: "50%" }} />
          <div style={{ marginLeft: "1em", width: "100%" }}>
            <div style={{ float: "right" }}><IonText color="light"><IonIcon icon={leafOutline} ></IonIcon> 45</IonText></div>
            <IonCardTitle color="light" >{username}</IonCardTitle>
            <IonCardSubtitle color="light" >2/4 Goals completed</IonCardSubtitle>
          </div>
        </div>
      </IonCardHeader>
    </IonHeader>
  )
}
