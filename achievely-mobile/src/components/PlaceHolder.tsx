import React from 'react';

interface PlaceHolderProps {
  name: string
}

const PlaceHolder: React.FC<PlaceHolderProps> = ({name}) => {
  return (
    <div>{name}</div>
  )
}

export default PlaceHolder;