import React from 'react';
import { useSelector } from 'react-redux';
import { RootState } from '../store/rootReducer';
import { RouteComponentProps, withRouter, useLocation } from 'react-router';

import { IonContent, IonIcon, IonItem, IonLabel, IonList, IonListHeader, IonMenu, IonMenuToggle, IonToggle } from '@ionic/react';
import { calendarOutline, help, informationCircleOutline, logIn, logOut, person, personAdd, barChartOutline, receiptOutline, rocketOutline } from 'ionicons/icons';


interface Pages {
  title: string,
  path: string,
  icon: string,
  routerDirection?: string
}

const routes = {
  securedPages: [
    { title: 'Daily', path: '/tabs/daily', icon: receiptOutline },
    { title: 'Goals', path: '/tabs/goals', icon: rocketOutline },
    { title: 'Stats', path: '/tabs/stats', icon: barChartOutline },
    { title: 'Calendar', path: '/tabs/cal', icon: calendarOutline },
    { title: 'Account', path: '/account', icon: person },
    { title: 'Logout', path: '/loginout', icon: logOut },
    { title: 'Support', path: '/support', icon: help },
    { title: 'About', path: '/about', icon: informationCircleOutline }
  ],
  publicPages: [
    { title: 'Login', path: '/loginout', icon: logIn },
    { title: 'Support', path: '/support', icon: help },
    { title: 'About', path: '/about', icon: informationCircleOutline }
  ]
};

interface StateProps {

}

interface MenuProps extends RouteComponentProps, StateProps { }

const Menu: React.FC<MenuProps> = ({ history }) => {
  const session = useSelector(
    (state: RootState) => state.session
  );

  const location = useLocation();

  function renderListItems(list: Pages[]) {
    return list
      .filter(route => !!route.path)
      .map(p => (
        <IonMenuToggle key={p.title} auto-hide="false">
          <IonItem detail={false} routerLink={p.path} routerDirection="none" className={location.pathname.startsWith(p.path) ? 'selected' : undefined}>
            <IonIcon slot="start" icon={p.icon} />
            <IonLabel>{p.title}</IonLabel>
          </IonItem>
        </IonMenuToggle>
      ));
  }

  return (
    <IonMenu type="overlay" disabled={false} contentId="main">
      <IonContent forceOverscroll={false}>
        <IonList lines="none">
          <IonListHeader>Achievely</IonListHeader>
          {session.account ? renderListItems(routes.securedPages) : renderListItems(routes.publicPages)}
        </IonList>
      </IonContent>
    </IonMenu>
  );
};

export default withRouter(Menu);
