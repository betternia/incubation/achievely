import React from 'react';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store'

import { render, screen } from '@testing-library/react';
import App from './App';

describe('App', () => {

  const storeState = {
    session: { account: undefined },
    noemas: undefined,
  };
  const mockStore = configureMockStore();
  let store;

  beforeEach(() => {
    store = mockStore(storeState)
  });

  test('renders without crashing', () => {
    const { baseElement } = render(
      <Provider store={store}>
        <App />
      </Provider>);
    expect(baseElement).toBeDefined();
    expect(screen.getByText('Achievely')).toBeInTheDocument();

  });
});
