
export const appConfig = {
  name: 'Achievely',
  appId: 'com.achievely.mobile', // used for auth callback URL
  authLoginRedirectPath: '/loginredirect',
  authLogoutRedirectPath: '/logoutredirect',
}
