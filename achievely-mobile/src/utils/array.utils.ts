

export type CompFunc<T> = (lhs: T, rhs:T) => number;

/**
 * Finds the location which the element in the array is greater than the element provided 
 * 
 * @param arr 
 * @param el 
 * @param compFunc 
 */
export function findLoc<T>(arr: T[], el: T, compFunc: CompFunc<T>): number { 
  for (let i = 0; i < arr.length; i++) { 
      // console.log(`pos[${i}] compFunc= ${compFunc(arr[i], el)}` );
      if (compFunc(arr[i], el) > 0) {
          return i - 1; 
      }
  } 
  return arr.length; 
}

/**
 * Returns a new array with the element inserted into an ordered location.
 * Pre-condition: the array must be ordered.
 * 
 * @param el 
 * @param arr 
 * @param compFunc 
 */
export function insertOrderedImmutable<T>(arr: T[], el: T, compFunc: CompFunc<T>): T[] {
  const idx = findLoc(arr, el, compFunc);
  // arr.splice(idx + 1, 0, el); 

  return [...arr.slice(0, idx+1), el, ...arr.slice(idx+1)]; 
}

/**
 * Returns a new array with the element inserted into an ordered location.
 * Pre-condition: the array must be ordered.
 * 
 * @param el 
 * @param arr 
 * @param compFunc 
 */
export function insertOrdered<T>(arr: T[], el: T, compFunc: CompFunc<T>): T[] {
  // Below idx results in same value as findLoc(arr, el, compFunc);

  const isLarger = (curr: T) => compFunc(curr, el) > 0;
  let idx = arr.findIndex(isLarger) -1;
  idx = (idx >= -1) ? idx : arr.length;

  arr.splice(idx + 1, 0, el); 
  return arr;
}

export function arrSearch<T> (arr: T[], el: T, compFunc: CompFunc<T>, start: number = 0, end: number | undefined = undefined) : [number, T] | null 
{
  for (let idx = 0; idx < arr.length; idx++) {
    if (compFunc(arr[idx], el) === 0) {
      return [idx, arr[idx]];
    }
  }
  return null;
}    

/**
 * Binary search of an array
 * Pre-condition: the array must be sorted
 * BUG, it seems to fail when there are two items
 * 
 * @param arr 
 * @param el 
 * @param compFunc 
 * @param start 
 * @param end 
 */
export function binarySearch<T> (arr: T[], el: T, compFunc: CompFunc<T>, start: number = 0, end: number | undefined = undefined) : [number, T] | null{ 
       
  // Base Condition 
  if (!arr || arr.length === 0) return null;

  end = (end === undefined) ? arr.length-1 : end;
  if (start > end) return null; 
 
  // Find the middle index 
  const mid = Math.floor((start + end)/2); 
 
  const compResult = compFunc(arr[mid], el);

  // console.log(`start=${start}, end=${end}, mid=${mid}, result=${compResult}`);

  // Compare mid with given key x 
  if (compResult === 0) {
    return [mid, arr[mid]]; 
  }

  // If element at mid is greater than el, 
  // search in the left half of mid 
  else if(compResult > 0) {
      return binarySearch(arr, el, compFunc, start, mid-1); 
  } else {
      // If element at mid is smaller than x, 
      // search in the right half of mid 
      return binarySearch(arr, el, compFunc, mid+1, end); 
  }
} 
