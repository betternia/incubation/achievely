import { insertOrdered, binarySearch } from './array.utils'

interface TestStruct {
  date: string;
  content: string;
  priority?: number | null;
}

const createObj = (date: string, content: string, priority?: number): TestStruct => {
  return {
    date, content, priority
  };
} 
 

describe("array_utils", () => {

  describe("insertOrdered", () => {
    test('insertOrdered ascending by string', () => {

      let arr: TestStruct[] = [
        createObj(new Date(2020, 1, 4).toJSON(), 'dd', 4),
        createObj(new Date(2020, 1, 2).toJSON(), 'bb', 3),
        createObj(new Date(2020, 1, 1).toJSON(), 'aa', 2),
        createObj(new Date(2020, 1, 3).toJSON(), 'cc', 1),
        createObj(new Date(2020, 1, 5).toJSON(), 'cc', 1)
      ];

      let result: TestStruct[] = [];

      const dateAsc = (lhs: TestStruct, rhs: TestStruct) => {
        return lhs.date.localeCompare(rhs.date);
      }
      for(const el of arr ) {
        result = insertOrdered<TestStruct>(result, el, dateAsc);
      }

      let expected: TestStruct[] = [
        createObj(new Date(2020, 1, 1).toJSON(), 'aa', 2),
        createObj(new Date(2020, 1, 2).toJSON(), 'bb', 3),
        createObj(new Date(2020, 1, 3).toJSON(), 'cc', 1),
        createObj(new Date(2020, 1, 4).toJSON(), 'dd', 4),
        createObj(new Date(2020, 1, 5).toJSON(), 'cc', 1)
      ];
      
      expect(result).toEqual(expected);
    });

    test('insertOrdered descending by string', () => {

      let arr: TestStruct[] = [
        createObj(new Date(2020, 1, 4).toJSON(), 'dd', 4),
        createObj(new Date(2020, 1, 2).toJSON(), 'bb', 3),
        createObj(new Date(2020, 1, 1).toJSON(), 'aa', 2),
        createObj(new Date(2020, 1, 3).toJSON(), 'cc', 1)
      ];

      let result: TestStruct[] = [];

      const dateDesc = (lhs: TestStruct, rhs: TestStruct) => {
        return rhs.date.localeCompare(lhs.date);
      } 
      for(const el of arr ) {
        result = insertOrdered<TestStruct>(result, el, dateDesc);
      }

      let expected: TestStruct[] = [
        createObj(new Date(2020, 1, 4).toJSON(), 'dd', 4),
        createObj(new Date(2020, 1, 3).toJSON(), 'cc', 1),
        createObj(new Date(2020, 1, 2).toJSON(), 'bb', 3),
        createObj(new Date(2020, 1, 1).toJSON(), 'aa', 2),
      ];
      
      expect(result).toEqual(expected);
    });

    test('insertOrdered ascending by number', () => {

      let arr: TestStruct[] = [
        createObj(new Date(2020, 1, 4).toJSON(), 'dd', 4),
        createObj(new Date(2020, 1, 2).toJSON(), 'bb', 3),
        createObj(new Date(2020, 1, 1).toJSON(), 'aa', 2),
        createObj(new Date(2020, 1, 3).toJSON(), 'cc', 1)
      ];

      let result: TestStruct[] = [];

      const priorityAsc = (lhs: TestStruct, rhs: TestStruct) => {
        return lhs.priority! - rhs.priority!;
      }
      for(const el of arr ) {
        result = insertOrdered<TestStruct>(result, el, priorityAsc);
      }

      // console.log(JSON.stringify(result, null, 2));

      let expected: TestStruct[] = [
        createObj(new Date(2020, 1, 3).toJSON(), 'cc', 1),
        createObj(new Date(2020, 1, 1).toJSON(), 'aa', 2),
        createObj(new Date(2020, 1, 2).toJSON(), 'bb', 3),
        createObj(new Date(2020, 1, 4).toJSON(), 'dd', 4)
      ];
      
      expect(result).toEqual(expected);
    });


    test('insertOrdered ascending by number with null on bottom', () => {

      let arr: TestStruct[] = [
        createObj(new Date(2020, 1, 4).toJSON(), 'dd', 4),
        createObj(new Date(2020, 1, 2).toJSON(), 'bb', 3),
        createObj(new Date(2020, 1, 1).toJSON(), 'aa'),
        createObj(new Date(2020, 1, 3).toJSON(), 'cc', 1)
      ];

      let result: TestStruct[] = [];

      const priorityAsc = (lhs: TestStruct, rhs: TestStruct) => {
        if (lhs.priority === null) {
          return 1
        }
        return lhs.priority! - rhs.priority!;
      }
      for(const el of arr ) {
        result = insertOrdered<TestStruct>(result, el, priorityAsc);
      }

      // console.log(JSON.stringify(result, null, 2));

      let expected: TestStruct[] = [
        createObj(new Date(2020, 1, 3).toJSON(), 'cc', 1),
        createObj(new Date(2020, 1, 2).toJSON(), 'bb', 3),
        createObj(new Date(2020, 1, 4).toJSON(), 'dd', 4),
        createObj(new Date(2020, 1, 1).toJSON(), 'aa'),
      ];
      
      expect(result).toEqual(expected);
    });

    test.skip('insertOrdered ascending by number with null on top', () => {

      let arr: TestStruct[] = [
        createObj(new Date(2020, 1, 4).toJSON(), 'dd', 4),
        createObj(new Date(2020, 1, 2).toJSON(), 'bb', 3),
        createObj(new Date(2020, 1, 1).toJSON(), 'aa'),
        createObj(new Date(2020, 1, 3).toJSON(), 'cc', 1)
      ];

      let result: TestStruct[] = [];

      const priorityAsc = (lhs: TestStruct, rhs: TestStruct) => {
        if (lhs.priority === null || lhs.priority === undefined) {
          return -1
        }
        return lhs.priority! - rhs.priority!;
      }
      for(const el of arr ) {
        // console.log(`Inserting [${el}]` );
        result = insertOrdered<TestStruct>(result, el, priorityAsc);
      }

      console.log(JSON.stringify(result, null, 2));

      let expected: TestStruct[] = [
        createObj(new Date(2020, 1, 1).toJSON(), 'aa'),
        createObj(new Date(2020, 1, 3).toJSON(), 'cc', 1),
        createObj(new Date(2020, 1, 2).toJSON(), 'bb', 3),
        createObj(new Date(2020, 1, 4).toJSON(), 'dd', 4),
      ];
      
      expect(result).toEqual(expected);
    });
  });

  describe("binarySearch", () => {
    test('GIVEN array is empty, WHEN search by string', () => {
      let arr: TestStruct[] = [];

      const el = createObj(new Date(2020, 1, 7).toJSON(), '');
      
      const dateAsc = (lhs: TestStruct, rhs: TestStruct) => {
        return lhs.date.localeCompare(rhs.date);
      }

      const result = binarySearch(arr, el, dateAsc);

      expect(result).toBeNull();
    });

    test('GIVEN array has one, WHEN search by string', () => {
      let arr: TestStruct[] = [
        createObj(new Date(2020, 1, 7).toJSON(), 'aa', 2)
      ];

      const el = createObj(new Date(2020, 1, 7).toJSON(), '');
      
      const dateAsc = (lhs: TestStruct, rhs: TestStruct) => {
        return lhs.date.localeCompare(rhs.date);
      }

      const result = binarySearch(arr, el, dateAsc);

      expect(result![0]).toEqual(0);
      expect(result![1]).toEqual(arr[0]);
    });

    test('GIVEN match exists, WHEN search by string', () => {
      let arr: TestStruct[] = [
        createObj(new Date(2020, 1, 1).toJSON(), 'aa', 2),
        createObj(new Date(2020, 1, 2).toJSON(), 'bb', 3),
        createObj(new Date(2020, 1, 3).toJSON(), 'cc', 1),
        createObj(new Date(2020, 1, 4).toJSON(), 'dd', 4),
        createObj(new Date(2020, 1, 7).toJSON(), 'dd', 4),
        createObj(new Date(2020, 1, 12).toJSON(), 'dd', 4)
      ];

      const el = createObj(new Date(2020, 1, 2).toJSON(), '');
      
      const dateAsc = (lhs: TestStruct, rhs: TestStruct) => {
        return lhs.date.localeCompare(rhs.date);
      }

      const result = binarySearch(arr, el, dateAsc);

      expect(result![0]).toEqual(1);
      expect(result![1]).toEqual(arr[1]);
    });

    test('GIVEN descending, and match exists, WHEN search by string', () => {
      let arr: TestStruct[] = [
        createObj(new Date(2020, 1, 12).toJSON(), 'dd', 4),
        createObj(new Date(2020, 1, 7).toJSON(), 'dd', 4),
        createObj(new Date(2020, 1, 4).toJSON(), 'dd', 4),
        createObj(new Date(2020, 1, 3).toJSON(), 'cc', 1),
        createObj(new Date(2020, 1, 2).toJSON(), 'bb', 3),
        createObj(new Date(2020, 1, 1).toJSON(), 'aa', 2)
      ];

      const el = createObj(new Date(2020, 1, 7).toJSON(), '');
      
      const dateDesc = (lhs: TestStruct, rhs: TestStruct) => {
        return rhs.date.localeCompare(lhs.date);
      }

      const result = binarySearch(arr, el, dateDesc);

      expect(result![0]).toEqual(1);
      expect(result![1]).toEqual(arr[1]);
    });

    test('GIVEN match does not exists, WHEN search by string', () => {
      let arr: TestStruct[] = [
        createObj(new Date(2020, 1, 12).toJSON(), 'dd', 4),
        createObj(new Date(2020, 1, 7).toJSON(), 'dd', 4),
        createObj(new Date(2020, 1, 4).toJSON(), 'dd', 4),
        createObj(new Date(2020, 1, 3).toJSON(), 'cc', 1),
        createObj(new Date(2020, 1, 2).toJSON(), 'bb', 3),
        createObj(new Date(2020, 1, 1).toJSON(), 'aa', 2),
      ];

      const el = createObj(new Date(2020, 1, 5).toJSON(), '');
      
      const dateDesc = (lhs: TestStruct, rhs: TestStruct) => {
        return rhs.date.localeCompare(lhs.date);
      }

      const result = binarySearch(arr, el, dateDesc);

      expect(result).toBeNull();
    });
  });
});
