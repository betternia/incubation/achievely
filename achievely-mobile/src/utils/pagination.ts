export interface Order {
  direction: string;
  property: string;
}

export interface Sort {
  orders: Array<Order>;
}

export interface Page<T> {
  /**
   * The number of the current page
   */
  number: number;

  /**
   * Size of the page (based on requested value)
   */
  size: number;

  /**
   * Number of elements in the current page
   */
  numberOfElements: number;

  /**
   * Total number of elements available
   */
  totalElements: number;

  /**
   * Total number of pages available
   */
  totalPages: number;

  /**
   * Sort of this page
   */
  sort?: Sort | undefined;

  /**
   * True if this is the first page
   */
  first?: boolean;

  /**
   * True if this is the last page in the available data set
   */
  last?: boolean;

  content: T[];
}
