import { isPlatform } from '@ionic/react';
import { Plugins } from '@capacitor/core';
import { AuthObserver, ConsoleLogObserver, AuthService as AppAuthService, AuthActions } from 'ionic-appauth';
import { CapacitorBrowser, CapacitorSecureStorage } from 'ionic-appauth/lib/capacitor';

import { appConfig } from '../app.config';
import { RequestService } from './request.service';
import { AxiosRequestor } from './axios.service';
import { AppDispatch } from '../store';

import * as userService from '../features/session/user.service';
import { normalizeUser } from '../features/session/okta.adapter';
import { setSession } from '../features/session/session.slice';
import { UserAccount } from '../features/session/session.types';

import { reloadUserData } from '../features/session/user.service';


const { App } = Plugins;

const AUTH_CLIENT_ID: string = process.env.REACT_APP_AUTH_CLIENT_ID || '';
const AUTH_SERVER_HOST = process.env.REACT_APP_AUTH_SERVER_HOST || '';
const AUTH_SCOPES = process.env.REACT_APP_AUTH_SCOPES || 'openid profile';

// Used by Auth0: needed to obtain API usable access_token
const AUTH_AUDIENCE = process.env.REACT_APP_AUTH_AUDIENCE || undefined;
// Used by Okta: needed if you want to use a specific social login (e.g. Facebook)
const AUTH_IDP = process.env.REACT_APP_AUTH_EXTRA_IDP || undefined;

// console.debug('AUTH_CLIENT_ID: ', AUTH_CLIENT_ID);
// console.debug('AUTH_SERVER_HOST: ', AUTH_SERVER_HOST);
// console.debug('AUTH_SCOPES: ', AUTH_SCOPES);

const capacitorLoginUrl = `${appConfig.appId}:/${appConfig.authLoginRedirectPath}`;
const capacitorLogoutUrl = `${appConfig.appId}:/${appConfig.authLogoutRedirectPath}`;

export class AuthService {

  public static isSigningIn = false;

  private static authService: AppAuthService | undefined;

  private static observer: AuthObserver;

  private static buildAuthInstance(): AppAuthService {
    const authService = new AppAuthService(new CapacitorBrowser(), new CapacitorSecureStorage(), new AxiosRequestor());
    authService.authConfig = {
      client_id: AUTH_CLIENT_ID,
      server_host: AUTH_SERVER_HOST,
      redirect_url: isPlatform('capacitor') ? capacitorLoginUrl : window.location.origin + appConfig.authLoginRedirectPath,
      end_session_redirect_url: isPlatform('capacitor') ? capacitorLogoutUrl : window.location.origin + appConfig.authLogoutRedirectPath,
      scopes: AUTH_SCOPES,
      pkce: true
    }

    if (isPlatform('capacitor')) {
      console.log("AuthService: listener created");
      App.addListener('appUrlOpen', (data: any) => {
        console.log('App.addListener data' + JSON.stringify(data, null, 2));
        console.log('App.addListener authConfig.redirect_url:' + authService.authConfig.redirect_url);
        if (data.url !== undefined) {
          if (data.url.startsWith(capacitorLoginUrl)) {
            console.log('App.addListener Logging IN');
            authService.authorizationCallback(data.url);
          } else if (data.url.startsWith(capacitorLogoutUrl)) {
            console.log('App.addListener Logging OUT');
            authService.endSessionCallback();
          }
        }
      });
    }

    authService.addActionObserver(new ConsoleLogObserver());
    return authService;
  }


  public static get Instance(): AppAuthService {
    if (!this.authService) {
      this.authService = this.buildAuthInstance();
    }

    return this.authService;
  }

  public static signIn() {
    console.log('LoginButtons:signIn...');

    let authExtra: any = {};
    if (AUTH_AUDIENCE) {
      authExtra.audience = AUTH_AUDIENCE
    }
    if (AUTH_IDP) {
      authExtra.idp = AUTH_IDP
    }    
    
    AuthService.Instance.signIn(authExtra);
  }

  public static async appStorageSet<T>(key: string, value: T): Promise<void> {
    return Plugins.Storage.set({key, value: JSON.stringify(value)});
  }

  public static async appStorageGet<T>(key: string): Promise<T> {
    const value = await Plugins.Storage.get({key});
    const strVal: string =  value.value === null ? 'null' : value.value;
    return JSON.parse(strVal) as T;
  }

  public static signOut() {
    console.log('AuthService:signOut...');
    
    (async () => {
      await AuthService.appStorageSet<boolean>('isSigningOut', true);
      AuthService.Instance.signOut();
    })();
  }

  // Sets the token
  public static setToken(accessToken: string) {
    RequestService.setDefaultHeader('Authorization', 'Bearer ' + accessToken);
  }

  public static listenAuthActions(dispatch: AppDispatch): void {
    // console.log(AuthService.Instance.session.isAuthenticated ? 'Auth' : 'Unauth');
    AuthService.observer = AuthService.Instance.addActionListener((action) => {
      console.log('AuthService/addActionListener action:' + JSON.stringify(action));
      if (action.action === AuthActions.SignInSuccess) {
        AuthService.signInOrRegister(dispatch).catch(error => {
          console.error('AuthService:listenAuthActions Error:' + error);
        });
        // history.replace('home');
      } else if (action.action === AuthActions.SignInFailed) {
        // When sign in failed
      } else if (action.action === AuthActions.LoadTokenFromStorageSuccess) {
        (async () => {
          const isSigningOut = await AuthService.appStorageGet<boolean>('isSigningOut');
          console.log('LoadTokenFromStorageSuccess isSigningOut:' + (isSigningOut ? 'true': 'false'));
          if (isSigningOut) {
            console.log('During sign-out flow, ignoring LoadTokenFromStorageSuccess');
            return; // 
          }
          console.log('Session:starting...')
          // TODO: DEFECT - on page refresh, it loads the data
          AuthService.setToken(AuthService.Instance.session.token?.accessToken!);
          userService.fetchUserAccount().then(account => {
            AuthService.startSession(dispatch, account);
            console.log('Session:started.')
          });
          // history.replace('home');
        })();
      } else if (action.action === AuthActions.SignOutSuccess) {
        dispatch(setSession(undefined));
        // history.replace('landing');
        AuthService.appStorageSet('isSigningOut', false);
      } else if (action.action === AuthActions.SignOutFailed) {
        // When sign in failed
        AuthService.appStorageSet('isSigningOut', false);
      }
    });
  }

  public static removeAuthActionListener() {
    AuthService.Instance.removeActionObserver(AuthService.observer);
  }

  /**
   * Signs in or registers
   * Precondition: the signInSuccessful
   * @param dispatch
   */
  public static async signInOrRegister(dispatch: AppDispatch): Promise<UserAccount | undefined> {

    if (!AuthService.Instance.session.isAuthenticated) {
      throw new Error('Not authenticated');
    }

    // Loading the userInfo to register at the server
    console.debug('AuthService:signInOrRegister setToken(' + AuthService.Instance.session.token?.accessToken! + ")");
    AuthService.setToken(AuthService.Instance.session.token?.accessToken!);

    const token = await AuthService.Instance.getValidToken();
    console.debug('token:' + JSON.stringify(token));

    console.debug('AuthService:signInOrRegister pre loadUserInfo()');
    await AuthService.Instance.loadUserInfo();
    console.debug('user:' + JSON.stringify(AuthService.Instance.session.user));

    const user = normalizeUser(AuthService.Instance.session.user);
    console.debug('AuthService:signInOrRegister pre userService.registerWithIdToken(%j)', user);
    const account = await userService.registerWithIdToken(AuthService.Instance.session.token?.idToken!, user);

    account && await AuthService.startSession(dispatch, account);

    return account;
  }

  public static async startSession(dispatch: AppDispatch, account: UserAccount) {
    console.debug(`AuthService:startSession setSession(${JSON.stringify(account, null, 2)})`);
    dispatch(setSession(account));
    await reloadUserData(dispatch);
  }
}
