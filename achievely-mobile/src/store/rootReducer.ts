import { combineReducers } from '@reduxjs/toolkit';

// Import your reducers here
import session from '../features/session/session.slice'
import noemas from '../features/noema/noema.slice'
import dailies from '../features/daily/daily.slice'

const rootReducer = combineReducers({ session, noemas, dailies/* add reducers */});

export type RootState = ReturnType<typeof rootReducer>;

export default rootReducer;
