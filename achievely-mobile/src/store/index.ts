import {configureStore, Action, getDefaultMiddleware} from '@reduxjs/toolkit'
import { ThunkAction } from 'redux-thunk'
import rootReducer, { RootState } from './persistentRootReducer'


// https://github.com/rt2zz/redux-persist/issues/988#issuecomment-654875104
const store = configureStore({
  reducer: rootReducer,
  middleware: getDefaultMiddleware({
    serializableCheck: {
      ignoredActions: ["persist/PERSIST"]
    }
  })

});

export type AppDispatch = typeof store.dispatch
export type AppThunk = ThunkAction<any, RootState, null, Action<string>>

export default store;
