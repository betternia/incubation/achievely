import { combineReducers } from '@reduxjs/toolkit';

// From https://stackoverflow.com/questions/63761763/how-to-configure-redux-persist-with-redux-toolkit
import storage from 'redux-persist/lib/storage'
import { persistReducer } from 'redux-persist'

// Import your reducers here
import session from '../features/session/session.slice'
import noemas from '../features/noema/noema.slice'
import dailies from '../features/daily/daily.slice'

const rootReducer = combineReducers({ session, noemas, dailies/* add reducers */});

// From: https://github.com/rt2zz/redux-persist/issues/988#issuecomment-638073306
const persistConfig = {
  key: 'root',
  storage
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

export type RootState = ReturnType<typeof rootReducer>;

export default persistedReducer;
