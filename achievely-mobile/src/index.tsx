import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import * as serviceWorkerRegistration from './serviceWorkerRegistration';
import reportWebVitals from './reportWebVitals';

import { PersistGate } from 'redux-persist/integration/react'
import { persistStore } from 'redux-persist'

import store from './store'

// Conditionally execute during development
if (process.env.NODE_ENV === 'test') {
  // For the Mock Service Worker
  const { worker } = require('./mocks/browser');
  worker.start();

  // Load noemas
  // const dailySlice = require('./features/daily/daily.slice')
  // import('./features/noema/noema.service')
  // .then(noemaService => {
  //   return noemaService.fetchNoemas();
  // })
  // .then(noemas => {
  //   for (const noema of noemas.content) {
  //     if (noema.kind === 'noema') {
  //       store.dispatch(dailySlice.addDayNoema(noema));
  //     } else {
  //       store.dispatch(dailySlice.addDayTask(noema));
  //     } 
  //   }
  // })
  // .catch(error => {
  //   console.error('Error while loading test data: %j', error);
  // });
}

let persistentStore = persistStore(store);

function render() {
  // This is just following the hot reload sample
  const App = require('./App').default
  ReactDOM.render(
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistentStore}>
        <App />
      </PersistGate>
    </Provider>,
    document.getElementById('root'));
}

render();


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://cra.link/PWA

// 2012/02/19 Following line disabled for MSW to work
// serviceWorkerRegistration.unregister();

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
