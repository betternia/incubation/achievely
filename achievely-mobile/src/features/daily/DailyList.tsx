import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../store/rootReducer';
import { Noema, Task, NoemaStatus} from '../noema/noema.types'
import NoemaListItem from '../noema/NoemaListItem';
import TaskListItem from '../noema/TaskListItem';

import { removeNoema } from '../noema/noema.slice';

import { IonItemGroup, IonItemDivider, IonLabel, IonList } from '@ionic/react';

import './DailyList.scss'

export default function DailyList() {

  const dispatch = useDispatch();

  const dailies = useSelector(
    (state: RootState) => state.dailies
  );

  // noemas is the id-based system of records
  const noemas = useSelector(
    (state: RootState) => state.noemas
  );

  const getEntrySource = (uid: string): Noema => {
    return noemas.byIds[uid];
  }

  function handleDelete(item: Noema) {
    dispatch(removeNoema(item));
  }

  return (
    <IonList data-qa="content-list">
      {dailies.map((daily, index: number) => (
        <IonItemGroup key={`group-${index}`}>
          <IonItemDivider sticky>
            <IonLabel color="tertiary" className="day-group">
              {daily.date}
            </IonLabel>
          </IonItemDivider>

          { daily.tasks && daily.tasks!.map((task: Task, taskIndex: number) => (
            <TaskListItem
              key={`item-${task.uid}`}
              item={getEntrySource(task.uid!)}
            />
          ))}
          {daily.noemas && daily.noemas!.map((noema: Noema, noemaIndex: number) => {
            const item = getEntrySource(noema.uid!)
            return (
              (item?.status !== NoemaStatus.DELETED) && 
              <NoemaListItem
                key={`item-${noema.uid}`}
                item={getEntrySource(noema.uid!)}
                onDelete={() => handleDelete(noema)}
              />
            )
          })}
        </IonItemGroup>
      ))}
    </IonList>
  )
}
