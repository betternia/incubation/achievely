import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { AppThunk, AppDispatch } from '../../store'

import { insertOrdered, arrSearch, binarySearch } from '../../utils/array.utils'

import { Noema, Task } from '../noema/noema.types'
import { resetNoemasState, addNoema, updateNoema, addTask, updateTask } from '../noema/noema.slice'

import { Daily } from './daily.types'


const initialState: Daily[] = [];

const dailyDateDesc = (lhs: Daily, rhs: Daily) => {
  return rhs.date.localeCompare(lhs.date);
}

const noemaEventDateDesc = (lhs: Noema, rhs: Noema) => {
  return rhs.eventDate.localeCompare(lhs.eventDate);
}

const dailySlice = createSlice({
  name: 'dailies',
  initialState,
  reducers: {
    resetDailyState(state) {
      return initialState
    },
    addDaily(state, action: PayloadAction<Daily>) {
      // Notice the state.push(), Immer look after immutability
      state.push(action.payload);
    },
    addDayNoema(state, action: PayloadAction<Noema>) {
      const newDaily: Daily = {
        date: action.payload.eventDate.substring(0, 10),
      }
      const searchRes = arrSearch(state, newDaily, dailyDateDesc);
      if (searchRes !== null) {
        console.log(`For (${action.payload.eventDate}), a matching Daily was found, add it to it`);
        insertOrdered(searchRes[1].noemas!, action.payload, noemaEventDateDesc);
      } else {
        console.log(`For (${action.payload.eventDate}), create a new Daily and add it`);
        newDaily.tasks = [];
        newDaily.noemas = [action.payload];
        const isLarger = (daily: Daily) => dailyDateDesc(daily, newDaily);
        let idx = state.findIndex(isLarger) - 1;
        idx = (idx >= -1) ? idx : state.length;
        state.splice(idx + 1, 0, newDaily);
      }

      return state;
    },
    updateDayNoema(state, action: PayloadAction<Noema>) {
      // TODO
      // If the eventDate has changed, move it, 
      // else, nothing to change
    },
    addDayTask(state, action: PayloadAction<Task>) {
      const newDaily: Daily = {
        date: action.payload.eventDate!.substring(0, 10),
      }
      const searchRes = binarySearch(state, newDaily, dailyDateDesc);
      if (searchRes !== null) {
        // A matching Daily was found, add it to it
        insertOrdered(searchRes[1].tasks!, action.payload, noemaEventDateDesc);
      } else {
        // Create a new Daily and add it
        newDaily.noemas = [];
        newDaily.tasks = [action.payload];
        const isLarger = (daily: Daily) => dailyDateDesc(daily, newDaily);
        let idx = state.findIndex(isLarger) - 1;
        idx = (idx >= -1) ? idx : state.length;
        state.splice(idx + 1, 0, newDaily);
      }

      return state;
    },
    updateDayTask(state, action: PayloadAction<Noema>) {
      // TODO
      // If the eventDate has changed, move it, 
      // else, nothing to change
    },
  }
});

export const addDayNoema = (noema: Noema): AppThunk => async (dispatch: AppDispatch) => {
  const val = await dispatch<Noema>(addNoema(noema));
  // alert(`val: ${JSON.stringify(val, null, 2)}`);
  dispatch(dailySlice.actions.addDayNoema(val));
}

export const updateDayNoema = (noema: Noema): AppThunk => async (dispatch: AppDispatch) => {
  const origEntry = await dispatch(updateNoema(noema));
  // alert(`val: ${JSON.stringify(origEntry, null, 2)}`);

  // TODO: send both original and new so that the logic can move from Daily record accordingly
  dispatch(dailySlice.actions.updateDayNoema(noema));
}

export const addDayTask = (task: Task): AppThunk => async (dispatch: AppDispatch) => {
  const val = await dispatch<Task>(addTask(task));
  dispatch(dailySlice.actions.addDayTask(val));
}

export const updateDayTask = (task: Task): AppThunk => async (dispatch: AppDispatch) => {
  const origEntry = await dispatch(updateTask(task));
  // alert(`val: ${JSON.stringify(origEntry, null, 2)}`);

  // TODO: send both original and new so that the logic can move from Daily record accordingly
  dispatch(dailySlice.actions.updateDayTask(task));
}

export const resetDailyState = (): AppThunk => async (dispatch: AppDispatch) => {
  await dispatch(dailySlice.actions.resetDailyState());
  return dispatch(resetNoemasState());
}

export default dailySlice.reducer;
