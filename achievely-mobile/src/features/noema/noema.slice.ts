import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { AppThunk, AppDispatch } from '../../store'
import * as noemaService from './noema.service';
import { Noema, NoemaStatus, Task } from './noema.types'

interface NoemaState {
  byIds: {[key:string]: Noema | Task}
  loading: boolean
}

const initialState: NoemaState = { 
  byIds: {},
  loading: false,
}

const noemasSlice = createSlice({
  name: 'noemas',
  initialState,
  reducers: {
    resetNoemasState(state) {
      return initialState
    },
    setNoema(state, action: PayloadAction<Noema>) {
      if (!action.payload.uid) {
        throw new Error('Item uid not provided');
      }

      // Notice the state.push(), Immer look after immutability
      state.byIds[action.payload.uid!] = action.payload;
    },
    removeNoema(state, action: PayloadAction<Noema>) {
      if (!action.payload.uid) {
        throw new Error('Item uid not provided');
      }
      const deletedEl: Noema = {
        ...state.byIds[action.payload.uid!],
        status: NoemaStatus.DELETED, 
        dateModified: action.payload.dateModified
      };
      
      state.byIds[action.payload.uid!] = deletedEl;
    },
    toggleHighlight(state, action: PayloadAction<string>) {
      state.byIds[action.payload].highlighted = !state.byIds[action.payload].highlighted;
    }
  }
});

/**
 * Primes a new noema by adding createDate
 * @param noema 
 * @param kind 
 */
function prepNewNoema(noema: Noema, kind: string): Noema{

  if (process.env.NODE_ENV === 'test') {
    if (!noema.uid) { 
      // A simple unique ID generator
      noema.uid = Math.random().toString(36).substr(2, 9);
    }
  }

  noema.kind = kind;
  noema.status = NoemaStatus.ACTIVE;
  noema.dateCreated = (new Date()).toJSON();
  noema.dateModified = (new Date()).toJSON();
  return noema;
}

export const addNoema = (noema: Noema) : AppThunk => async(dispatch: AppDispatch) => {
  prepNewNoema(noema, 'noema');
  
  // If noema came from retrieval, just add, otherwise post it to the server first 
  const addedNoema = (noema.uid) ? noema : await noemaService.postNoema(noema);

  dispatch (noemasSlice.actions.setNoema(addedNoema));
  return addedNoema;
}

export const updateNoema = (noema: Noema) : AppThunk => async(dispatch: AppDispatch) => {
  noema.dateModified = (new Date()).toJSON();
  
  const modifiedNoema = await noemaService.putNoema(noema);

  dispatch (noemasSlice.actions.setNoema(modifiedNoema));
  return modifiedNoema; 
}

export const removeNoema = (noema: Noema) : AppThunk => async(dispatch: AppDispatch) => {  
  await noemaService.deleteNoema(noema);

  dispatch (noemasSlice.actions.removeNoema(noema));
}

export const addTask = (task: Task) : AppThunk => async(dispatch: AppDispatch) => {
  prepNewNoema(task, 'task');

  const addedTask =  (task.uid) ? task : await noemaService.postNoema(task);
  
  dispatch (noemasSlice.actions.setNoema(addedTask));
  return addedTask;
}

export const updateTask = (task: Task) : AppThunk => async(dispatch: AppDispatch) => {
  task.dateModified = (new Date()).toJSON();
  
  const modifiedTask = await noemaService.putNoema(task);

  dispatch (noemasSlice.actions.setNoema(task));
  return modifiedTask; 
}

export const { resetNoemasState, toggleHighlight } = noemasSlice.actions;

export default noemasSlice.reducer;
