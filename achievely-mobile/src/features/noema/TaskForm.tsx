import React from 'react';
import { useDispatch } from  'react-redux'
import { useForm } from 'react-hook-form';

import { IonItem, IonLabel, IonInput, IonTextarea, IonButton, IonFab, IonFabButton, IonIcon } from '@ionic/react';
import { checkmarkSharp } from 'ionicons/icons';

import { addDayTask, updateDayTask } from '../daily/daily.slice';
import { Task } from './noema.types';

interface TaskFormProps {
  entry?: Task,
  onComplete: () => void
}

export default function TaskForm({entry, onComplete}: TaskFormProps): JSX.Element {
  const dispatch = useDispatch();

  const { register, setValue, handleSubmit, errors } = useForm<Task>();

  const onSubmit = handleSubmit( (formData) => {
    // console.log(firstName, lastName);
    formData.uid = entry?.uid;
    formData.status = formData.status;
    formData.kind = 'task';
    formData.highlighted = entry?.highlighted;
    formData.eventDate = entry ? entry.eventDate : (new Date()).toJSON();
    
    if (entry) {
      dispatch(updateDayTask(formData));
    } else {
      dispatch(addDayTask(formData));
    }
    onComplete();
  });

  return (
    <form onSubmit={onSubmit}>
      <IonItem>
        {/* <IonLabel position="stacked" color="primary">Kind</IonLabel>
        <IonInput name="kind" type="text" ref={register} 
          placeholder="kind"
          autocapitalize="off" required>
        </IonInput> */}
        <IonLabel position="stacked" color="primary">Title</IonLabel>
        <IonInput name="title" type="text" ref={register}
          placeholder="title"
          autocapitalize="off" required>
        </IonInput>
        <IonLabel position="stacked" color="primary">Content</IonLabel>
        <IonTextarea name="content" ref={register}
          placeholder="content"
          autocapitalize="off" rows={5} required>
        </IonTextarea>
        <IonLabel position="stacked" color="primary">Tags</IonLabel>
      </IonItem>
      <IonFab slot="fixed" vertical="bottom" horizontal="center">
        {/* <IonFabButton color="primary" type="submit"> */}
        <IonButton type="submit" >
          <IonIcon icon={checkmarkSharp} />
        </IonButton>
        {/* </IonFabButton> */}
      </IonFab>
    </form>
  )
} 
