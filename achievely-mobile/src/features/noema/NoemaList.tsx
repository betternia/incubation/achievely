import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../store/rootReducer';

import { IonList } from '@ionic/react';

import { removeNoema } from './noema.slice';
import { Noema, NoemaStatus } from './noema.types';
import NoemaListItem from './NoemaListItem';

export default function NoemaList() {

  const dispatch = useDispatch();

  const noemas = useSelector(
    (state: RootState) => Object.values(state.noemas.byIds)
      .filter(el => el.status !== NoemaStatus.DELETED)
  );

  function handleDelete(item: Noema) {
    dispatch(removeNoema(item));
  }

  return (
    <IonList>
      {Object.values(noemas).map((item, index: number) => (
        <NoemaListItem
          key={`item-${item.uid}`}
          item={item}
          onDelete={() => handleDelete(item)}
        />
      ))}
    </IonList>
  )
}
