import React, { useRef } from 'react';

import { useHistory } from "react-router-dom";

import { Task } from './noema.types'

import { IonIcon, IonItemSliding, IonItem, IonLabel, IonNote, IonItemOptions, IonItemOption } from '@ionic/react';
import { squareOutline, checkboxOutline, createOutline, star } from 'ionicons/icons';

import './TaskListItem.scss';

interface TaskProps {
  item: Task
}

export default function TaskListItem({ item }: TaskProps) {
  const history = useHistory();
  const ionItemSlidingRef = useRef<HTMLIonItemSlidingElement>(null)

  return (
    <IonItemSliding className="task-item" ref={ionItemSlidingRef} >
      <IonItem >
        <IonItem lines="none" style={{ width: "100%" }} onClick={() => history.push(`/entry/${item.uid}`)} >
          {item.completedDate ? <IonIcon icon={checkboxOutline} />
            : <IonIcon icon={squareOutline} />}
          <IonLabel  data-qa="task-list-item">
            {/* <h4>{DateTime.fromISO(item.eventDate!).toLocaleString(DateTime.TIME_SIMPLE)}</h4> */}
            <h3>{item.title}</h3>
            <p>
              {item.content}
            </p>
          </IonLabel>
        </IonItem>

        <IonNote slot="end" onClick={() => history.push(`/edit/${item.uid}`)}>
          {item.highlighted && <IonIcon color="warning" icon={star}></IonIcon>}
          <IonIcon icon={createOutline} title="Edit"></IonIcon>
        </IonNote>
      </IonItem>
      <IonItemOptions side="start">
        <IonItemOption color="danger">
          Blocked
          </IonItemOption>
      </IonItemOptions>
      <IonItemOptions side="end">
        <IonItemOption color="success">
          Completed
          </IonItemOption>
      </IonItemOptions>
    </IonItemSliding>
  )
}
