import React, { useRef } from 'react';
import { useHistory } from "react-router-dom";
import * as dateUtils from '../../utils/date.utils';

import { IonIcon, IonItemSliding, IonItem, IonLabel, IonNote, IonItemOptions, IonItemOption } from '@ionic/react';
import { createOutline, star } from 'ionicons/icons';

import { Noema } from './noema.types'
import './NoemaListItem.scss';

interface NoemaProps {
  item: Noema
  onDelete: (item: Noema) => void
}

export default function NoemaListItem({ item, onDelete }: NoemaProps) {

  const history = useHistory();
  const ionItemSlidingRef = useRef<HTMLIonItemSlidingElement>(null)

  return (
    <IonItemSliding className="noema-item" ref={ionItemSlidingRef} >
      <IonItem >
        <IonItem lines="none" style={{ width: "100%" }} onClick={() => history.push(`/entry/${item.uid}`)} >
          <IonLabel data-qa="noema-list-item">
            <h4>{dateUtils.formatDateTime(item.eventDate!)}</h4>
            <h3>{item.title}</h3>
            <p>{item.content}</p>
          </IonLabel>
        </IonItem>

        <IonNote slot="end" onClick={() => history.push(`/edit/${item.uid}`)}>
          {item.highlighted && <IonIcon color="warning" icon={star}></IonIcon>}
          <IonIcon icon={createOutline} title="Edit"></IonIcon>
        </IonNote>
      </IonItem>

      <IonItemOptions side="start">
        <IonItemOption color="danger" onClick={() => onDelete(item)}>
          Delete
        </IonItemOption>
      </IonItemOptions>
    </IonItemSliding>
  )
}
