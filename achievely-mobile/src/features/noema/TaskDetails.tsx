import React from 'react';
import { Task } from './noema.types';
import { DateTime } from 'luxon';

import { IonItem, IonChip, IonLabel, IonIcon, IonText } from '@ionic/react';
import { happyOutline } from 'ionicons/icons';

interface TaskDetailsProps {
  entry: Task
}

export default function TaskDetails({entry}: TaskDetailsProps): JSX.Element {

  const datePart = DateTime.fromISO(entry.eventDate!).toLocaleString();
  const timePart = DateTime.fromISO(entry.eventDate!).toLocaleString(DateTime.TIME_SIMPLE);
  
  return (
    <>
      <IonLabel>
        <div style={{ padding: "7px", display: "flex", backgroundColor: "#F8F9F9" }}>
          <div style={{ padding: "5px" }} >
            <IonIcon icon={happyOutline} title="Edit" style={{ height: "30px", color: "red" }} ></IonIcon>
            </div>
          <div style={{ padding: "5px" }} >
            <IonText color="primary">{datePart}</IonText><br />
            <IonText color="primary">{timePart}</IonText>
          </div>
          <IonText color="secondary"><h1 >{entry.title}</h1></IonText>
        </div>
      </IonLabel>
      <IonItem lines="none" className="ax-entry-content">
        {entry.content}
      </IonItem >
      {
        entry.tags && entry.tags.map(tag => (
          <IonChip color="dark" key={tag}>
            <IonLabel>{tag}</IonLabel>
          </IonChip>
        ))
      }
    </>
  )
} 
