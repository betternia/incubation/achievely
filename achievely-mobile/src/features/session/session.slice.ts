import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { AppThunk, AppDispatch } from '../../store'
import { resetDailyState } from '../daily/daily.slice';
import { UserAccount, Session } from './session.types'


const initialState: Session = {
  loading: false,
  account: undefined,
  loginTimestamp: undefined
};

const sessionSlice = createSlice({
  name: 'session',
  initialState,
  reducers: {
    setLoading(state/*, action: PayloadAction<boolean>*/) {
      state = { loading: true, account: undefined, loginTimestamp: undefined };
      return state;
    },

    setSession(state, action: PayloadAction<UserAccount | undefined>) {
      console.debug('[sessionSlice:setSession] orig state:', JSON.stringify(state, null, 2));
      state = { loading: false, account: action.payload, loginTimestamp: Date.now() };
      
      console.log('[sessionSlice:setSession] new state: ', state);
      return state;
    },

    removeSession(state) {
      state = initialState;
    }
  }
});

/**
 * Sets session and loads user data
 * @param account 
 */
export const setSession = (account: UserAccount | undefined): AppThunk => async (dispatch: AppDispatch) => {

  dispatch(sessionSlice.actions.setSession(account));
  if (account === undefined) {
    dispatch(resetDailyState());
  }
}

export const { setLoading, removeSession } = sessionSlice.actions;

export default sessionSlice.reducer;
