import { AppDispatch } from '../../store'

import { RequestService } from '../../utils/request.service';
import { addDayNoema, addDayTask, resetDailyState } from '../daily/daily.slice'
import { fetchNoemas } from '../noema/noema.service'
import { User, UserAccount } from './session.types';

const ROOT_PATH = process.env.REACT_APP_SERVICE_BASE_URL;
const MY_ACCOUNT_PATH = ROOT_PATH + '/my/account';
const MY_USER_PATH = ROOT_PATH + '/my/user';
const REGISTER_ACCOUNT_PATH = ROOT_PATH + '/account/register';

/**
 * Registers the account 
 * @param idToken The ID Token in form of JWT as obtained from social login 
 */
export async function registerWithIdToken(idToken: String, user: User): Promise<UserAccount | undefined> {
  const response = await RequestService.post<UserAccount>(`${REGISTER_ACCOUNT_PATH}`, { idToken: idToken, user });
  return response.data;
}

export async function fetchUserAccount(): Promise<UserAccount> {
  const response = await RequestService.get<UserAccount>(`${MY_ACCOUNT_PATH}`);
  return response.data;
}

export async function fetchUserInfo(): Promise<User> {
  const response = await RequestService.get<User>(`${MY_USER_PATH}`);
  return response.data;
}

export async function reloadUserData(dispatch: AppDispatch) {

  dispatch(resetDailyState());
  const noemasPage = await fetchNoemas();

  for (const noema of noemasPage.content) {
    if (noema.kind === 'noema') {
      dispatch(addDayNoema(noema));
    } else {
      dispatch(addDayTask(noema));
    }
  }
}
