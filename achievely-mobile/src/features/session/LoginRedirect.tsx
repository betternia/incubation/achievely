import React from 'react';
import { useDispatch } from 'react-redux'
import { AuthService } from '../../utils/auth.service';
import { AuthActions, AuthObserver } from 'ionic-appauth';
import { RouteComponentProps } from 'react-router';
import { useIonViewDidLeave, useIonViewDidEnter, IonPage } from '@ionic/react';


interface LoginRedirectPageProps extends RouteComponentProps { }

const LoginRedirect: React.FC<LoginRedirectPageProps> = (props: LoginRedirectPageProps) => {

  const dispatch = useDispatch();
  let observer: AuthObserver;

  useIonViewDidEnter(() => {
    AuthService.Instance.authorizationCallback(window.location.origin + props.location.pathname + props.location.search);
    // Factored in in AuthService
    observer = AuthService.Instance.addActionListener((action) => {
      console.log('LoginRedirect/addActionListener action:' + JSON.stringify(action));
      if (action.action === AuthActions.SignInSuccess) {
          props.history.replace('home');
      } else if (action.action === AuthActions.SignInFailed) {
        console.log('LoginRedirect: Going to landing')
        props.history.replace('landing');
      }
    });
  });

  useIonViewDidLeave(() => {
    AuthService.Instance.removeActionObserver(observer);
  });

  return (
    <IonPage>
      <p>Signing in...</p>
    </IonPage>
  );
};

export default LoginRedirect;
