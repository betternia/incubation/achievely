import React from 'react';
import { useDispatch } from 'react-redux'
import { AuthService } from '../../utils/auth.service';
import { RouteComponentProps } from 'react-router';
import { AuthActions, AuthObserver } from 'ionic-appauth';
import { useIonViewWillEnter, useIonViewDidLeave, IonPage } from '@ionic/react';

import { setSession } from  './session.slice'

interface LogoutRedirectPageProps extends RouteComponentProps { }

const LogoutRedirect: React.FC<LogoutRedirectPageProps> = (props: LogoutRedirectPageProps) => {

    let observer: AuthObserver;

    const dispatch = useDispatch();

    useIonViewWillEnter(() => {
        AuthService.Instance.endSessionCallback();
        // Actual auth logic factored in in AuthService
        observer = AuthService.Instance.addActionListener((action) => {
            console.log('LogoutRedirect/addActionListener action:' + JSON.stringify(action));
            if (action.action === AuthActions.SignOutSuccess ||
                action.action === AuthActions.SignInFailed
            ) {
                // Commented out for now, as it will redirect to landing which will also handle SignOutSuccess
                // dispatch(setSession(undefined));
                props.history.replace('landing');
            }
        });
    });

    useIonViewDidLeave(() => {
        AuthService.Instance.removeActionObserver(observer);
    });

    return (
        <IonPage>
            <p>Signing out...</p>
        </IonPage>
    );
};

export default LogoutRedirect;
