import React from 'react';
import { useDispatch } from 'react-redux'
import { useIonViewWillEnter, useIonViewDidLeave, IonButton } from '@ionic/react';
import { useHistory } from "react-router-dom";

import { AuthActions, AuthActionBuilder, AuthObserver } from 'ionic-appauth';

import { AuthService } from '../../utils/auth.service';
import { Session } from './session.types';
import { setSession } from  './session.slice'

interface LoginButtonsProp  {
  theme?: string;
  session: Session
}

export default function LoginButtons({ session }: LoginButtonsProp) {

  const dispatch = useDispatch();
  const history = useHistory();

  let observer: AuthObserver;

  function handleSignIn(e: any) {
    e.preventDefault();

    AuthService.signIn();
    // The logout flow is completed at LogoutRedirect.tsx
  }

  function handleSignOut(e?: any) {
    e.preventDefault();

    if (AuthService.Instance.session.isAuthenticated) {
      console.log('LoginButtons:handleSignOut signingOut...');
      AuthService.signOut();
      // The logout flow is completed at LogoutRedirect.tsx
    } else if (session.account) {
      console.log('LoginButtons:handleSignOut isAuthenticated is false, resetting redux session...');
      // It is possible that AuthService.Instance.session.isAuthenticated is
      // out of sync with store.session. In such case, the sign-out will not trigger
      // redirect to LogoutRedirect.tsx, hance state handled here
      // TODO: this means token has expired, the request will return 401
      dispatch(setSession(undefined));
      history.replace('landing');
    }
  }

  console.log('LoginButtons isAuthenticated: ' 
    + (AuthService.Instance.session.isAuthenticated ? 'true' : 'false')
    + ', account:' + JSON.stringify(session.account)
    );
  
  useIonViewWillEnter( async() => {
      // AuthService.Instance.loadTokenFromStorage();
      // alert(Auth.Instance.session.isAuthenticated ? 'Auth' : 'Unauth');
      // Factored in in AuthService
      observer = AuthService.Instance.addActionListener((action) => {
          console.log('LoginButtons/addActionListener action:' + JSON.stringify(action));
          if(action.action === AuthActions.SignInSuccess){
            history.replace('home');
          } else if(action.action === AuthActions.SignOutSuccess) {
            // TODO: Improve this logic. 
            // This bloc is here for the logout flow in mobile which does not go through LogoutRedirect
            // In web, the LogoutRedirect will return to landing, which contains this button, 
            // and try to redirect to landing again.
            // dispatch(setSession(undefined));
            history.replace('landing');
          }
      });
  });

  useIonViewDidLeave(() => {
    // AuthService.Instance.removeActionObserver(observer);
  });
  
  return (
    session.account || (AuthService.Instance.session.isAuthenticated) ? <div>
      <IonButton color="danger" onClick={handleSignOut} data-qa="logout-btn">
        Log Out
      </IonButton>
    </div> :
      <div>
        <IonButton color="light" fill="outline" onClick={handleSignIn} data-qa="login-btn">
          Log in
        </IonButton>
      </div>
  );
}
