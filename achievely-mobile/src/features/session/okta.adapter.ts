import { User } from './session.types'

/**
 * Given Okta user JSON, it will return into Achievely's 
 * @param user Okta User
 */
export function normalizeUser(user: any): User {
  return {
    id: user.preferred_username,
    givenName: user.given_name,
    familyName: user.family_name,
    alternateName: user.preferred_username,
    image: user.picture,
    locale: user.locale,
    dateModified: user.updated_at,
    email: user.email,
  };
}
