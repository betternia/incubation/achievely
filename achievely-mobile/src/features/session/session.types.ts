export interface UserAccount {
  uid?: string;
  dateCreated?: string;
  source: string;
  sourceAccountId: string;
  syncDate?: string;
  sourceProfile?: string;
  lastAccessDate?: string;

  user?: User;
  userUid?: string;
}

export interface User {
  uid?: string;
  dateCreated?: string;
  dateModified?: string;
  id: string;
  givenName: string;
  familyName: string;
  additionalName?: string;
  alternateName?: string;
  address?:string;
  description?: string;
  birthDate?: string;
  birthPlace?:string;
  image?: string;
  locale?: string;
  email?: string[];
}

export interface Session {
  loading: boolean,
  account: UserAccount | undefined,
  loginTimestamp: number | undefined, // epoch
}
