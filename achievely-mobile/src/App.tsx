import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux'
import { Redirect, Route } from 'react-router-dom';
import { IonApp, IonRouterOutlet, IonSplitPane } from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';

import { ProtectedRoute } from './components/ProtectedRoute';

import Menu from './components/Menu';
import Landing from './pages/Landing';
import MainTabs from './pages/MainTabs';
import EntryDetails from './pages/EntryDetails';
import EntryForm from './pages/EntryForm';

import LoginRedirect from './features/session/LoginRedirect';
import LogoutRedirect from './features/session/LogoutRedirect';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';
import LoginLogout from './pages/LoginLogout';
import About from './pages/About';
import { AuthService } from './utils/auth.service';

const App: React.FC = () => {

  const dispatch = useDispatch();

  useEffect(() => {
    // TODO: Fix, this is causing to setSession during logout flow
    console.log('App:useEffect loadTokenFromStorage...');
    AuthService.Instance.loadTokenFromStorage();
    AuthService.listenAuthActions(dispatch);

    return () => {
      AuthService.removeAuthActionListener();
    };
  }, []);

  return (
    <IonApp>
      <IonReactRouter>
        <IonSplitPane contentId="main">
          <Menu />
          <IonRouterOutlet id="main">
            <Route path="/landing" component={Landing} exact={true} />
            <Route path="/home" render={() => <Redirect to="/tabs/daily" />} exact={true} />
            <ProtectedRoute path="/tabs" component={MainTabs} />

            <ProtectedRoute path="/entry/:uid" component={EntryDetails} />
            <ProtectedRoute path="/new/:type" component={EntryForm} />
            <ProtectedRoute path="/edit/:uid" component={EntryForm} />

            <Route path="/loginout" component={LoginLogout} exact={true} />
            <Route path="/loginredirect" component={LoginRedirect} exact={true} />
            <Route path="/logoutredirect" component={LogoutRedirect} exact />
            <Route path="/" render={() => <Redirect to="/landing" />} exact={true} />

            <Route path="/about" component={About} exact={true} />
          </IonRouterOutlet>
        </IonSplitPane>
      </IonReactRouter>
    </IonApp>
  )
};

export default App;
