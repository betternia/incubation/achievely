import { handlers as userHandlers } from "./users.handlers";
import { handlers as noemaHandlers } from "./noemas.handlers";

export const handlers = [
  ...userHandlers,
  ...noemaHandlers,
];
