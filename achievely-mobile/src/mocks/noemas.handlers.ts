import { rest } from 'msw'

const noemas = require('./mock-data-noemas.json');

export const handlers = [
  // Handles a POST /login request
  rest.get('/noemas', (req, res, ctx) => {
    // Persist user's authentication in the session
    return res(
      // Respond with a 200 status code
      ctx.status(200),
      ctx.json(noemas)
    )
  }),
]

