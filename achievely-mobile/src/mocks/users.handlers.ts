import { rest } from "msw";



function newAccount(id: string = "MSW-user-id") {
  return {
    uid: "MSW-account-uid",
    dateCreated: "2021-02-19T23:10:55.551Z",
    source: "MSW-source",
    sourceAccountId: "MSW-account-id",
    syncDate: "2021-02-19T23:10:55.551Z",
    sourceProfile: undefined,
    lastAccessDate: undefined,

    user: {
      id: id,
      givenName: "MSW Kil Dong",
      familyName: "MSW Hong",
      image: undefined,
      locale: "en",
    },
    userUid: "MSW-user-uid",
  };
}

export const handlers = [
  // Handles a POST /login request
  rest.get("/my/account", (req, res, ctx) => {
    // Persist user's authentication in the session
    sessionStorage.setItem("is-authenticated", 'true');
    return res(
      // Respond with a 200 status code
      ctx.status(200),
      ctx.json(newAccount('MSW-user-myid'))
    );
  }),

  rest.post("/account/register", (req, res, ctx) => {
    // Persist user's authentication in the session
    sessionStorage.setItem("is-authenticated", 'true');
    console.log('[account/register] req: ', req.body);
    // const user = req.body?['user'];
    return res(
      // Respond with a 200 status code
      ctx.status(200),
      ctx.json(newAccount('MSW-user-newid'))
    );
  }),

  // Handles a GET /user request
  rest.get("/user", (req, res, ctx) => {
    // Check if the user is authenticated in this session
    const isAuthenticated = sessionStorage.getItem("is-authenticated");
    if (!isAuthenticated) {
      // If not authenticated, respond with a 403 error
      return res(
        ctx.status(403),
        ctx.json({
          errorMessage: "Not authorized",
        })
      );
    }
    // If authenticated, return a mocked user details
    return res(
      ctx.status(200),
      ctx.json({
        username: "admin",
      })
    );
  }),
];
