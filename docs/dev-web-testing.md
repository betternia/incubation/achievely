
# Testing with jest
## react-scripts + jest + TypeScript + ES6?
If the packages are built from TS, it will use ES6, JS by default needs CommonJS, you can ignore by running
```
react-scripts test --transformIgnorePatterns \"node_modules/(?!auth-browser)/\"
```
See: https://stackoverflow.com/questions/55493322/create-react-app-jest-encountered-unexpected-token

```
yarn workspaces run test  --watchAll=false 
```

### References
https://jestjs.io/docs/en/configuration

# Testing with Playwright

$[achievely-qa-automation] NODE_ENV=local yarn test {opts}
opts: headful, slowMo=100, screenshotOnFailure, video

WHen testing google login, you will need to disable
https://myaccount.google.com/lesssecureapps


https://github.com/microsoft/playwright-test

# Testing with Mock Service Worker
https://mswjs.io/docs/getting-started/integrate/browser


