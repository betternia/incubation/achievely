Configuring Auth0
================

If you have not created a free dev account in Auth0 yet, go to https://manage.auth0.com and create one.
(I am using kineart@gmail.com)

### Creating an Application

In order to connect your app to Auth0, you need to register an application in Auth0 first.

1. Go to "Application" and click the "+ CREATE APPLICATION" button.
2. Select "Single Page Applications"
2.1. Click on the "Settings" Tabe and edit the fields, the most important are:
  - Allowed callback URLs: `http://localhost:8100/loginredirect,com.achievely.mobile://callback`
  - Allowed Logout URL,: `http://localhost:8100/logoutredirect,com.achievely.mobile://logoutredirect`
  - Allowed Origins (CORS): `http://localhost`
  

### Adding a Social Identity Provider

This optional step allows you to use social media- Google, Facebook, etc - as the Identity Provider.

**NOTE** I am assuming you already have a project in [Google Dev Console](https://console.developers.google.com/) with OAuth2 enabled.

1. In Auth0 manage console, go to `Connections` -> `Social`
2. Click on "+ CREATE CONNECTION" and select "Google"
3. Give the IdP a name and copy paste the `Client ID` and `Client Secret` from Google Dev Console.
  - THe scopes should include `email, openid, profile`

Once completed 


## Configuring the Application

The application will pickup the configuration from the `.env` file.

.env file:
```shell
REACT_APP_AUTH_CLIENT_ID=your-client-id
REACT_APP_AUTH_SERVER_HOST=https://dev-xyz.Auth0.com
REACT_APP_AUTH_SCOPES="openid profile"
```

## Auth0 & end_session_endpoint
Auth0's OIDC wellknown does not return end_session_endpoint
ionic-appauth/lib/end-session-request-handler
```javascript
let baseUrl = configuration.endSessionEndpoint || 'https://kine-dev.us.auth0.com/v2/logout?client_id=S5wQ9VXR08ZAybSzY2ow6DnqbmL1RHEZ';
```

## Auth0 obtain session
https://manage.auth0.com/dashboard/us/kine-dev/apis/management/explorer
https://community.auth0.com/t/access-token-too-short-jwt-malformed/9169/8

> **NOTE**: You need to provide `audience` query parameter while calling the OIDC's `authorize` endpoint. You can provide it by sending extra parameter.
```javascript
  AuthService.Instance.signIn({ audience: AUTH_AUDIENCE });
```

## Authorization URL sample
- Raw:
```
https://kine-dev.us.auth0.com/authorize?audience=https%3A%2F%2Fkine-dev.us.auth0.com%2Fapi%2Fv2%2F&client_id=S5wQ9VXR08ZAybSzY2ow6DnqbmL1RHEZ&redirect_uri=http%3A%2F%2Flocalhost%3A8100&scope=openid%20profile%20email&response_type=code&response_mode=query&state=MWNOWXdaWEhpb1h4a21vRGJuNDE4RTZuTy5NbEh0WkhmTzVNM05YUnZ2Rw%3D%3D&nonce=ZlRhbl9lQzdyTExJc3lUSlB0c3FaNHNDSUFDSDlweE9JOVF%2BQi55VDQzLQ%3D%3D&code_challenge=eYNDviQ1MkbslzoyE-IKn0jRm-0DVp3LG1joZK7rQyc&code_challenge_method=S256&auth0Client=eyJuYW1lIjoiYXV0aDAtcmVhY3QiLCJ2ZXJzaW9uIjoiMS4yLjAifQ%3D%3D
```

- URLDECODED:
```
https://kine-dev.us.auth0.com/authorize?audience=https://kine-dev.us.auth0.com/api/v2/&client_id=S5wQ9VXR08ZAybSzY2ow6DnqbmL1RHEZ&redirect_uri=http://localhost:8100&scope=openid profile email&response_type=code&response_mode=query&state=MWNOWXdaWEhpb1h4a21vRGJuNDE4RTZuTy5NbEh0WkhmTzVNM05YUnZ2Rw==&nonce=ZlRhbl9lQzdyTExJc3lUSlB0c3FaNHNDSUFDSDlweE9JOVF+Qi55VDQzLQ==&code_challenge=eYNDviQ1MkbslzoyE-IKn0jRm-0DVp3LG1joZK7rQyc&code_challenge_method=S256&auth0Client=eyJuYW1lIjoiYXV0aDAtcmVhY3QiLCJ2ZXJzaW9uIjoiMS4yLjAifQ==
```

## APPENDIX Social Login IDP

###  Google client
Go to https://console.developers.google.com/
1. Create a project
2. IN the left menu, select `Credentials`
3. On top menu, press '+ CREATE CREDENTIALS' and chose Oauth Client ID
  Configure Consent Screen: External

NOTE: On mobile, on the very first login, it cannot return to the app page after credentials are submitted.
It works with Facebook. 

### Facebook
Go to https://developers.facebook.com/apps/
1. Click "Create App"
2. Settings; update values accordingly
3. Products -> Facebook Login / Settings:  
  - Valid OAuth Redirect URIs: `https://kine-dev.us.auth0.com/login/callback`
