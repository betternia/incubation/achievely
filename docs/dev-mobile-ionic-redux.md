Developing IONIC: Adding Redux with Redux Toolkit
=================================================

## Prepare the store
Create a directory `store` under `./src` and add `rootReducers.ts` and `index.ts` files.

Modify th `index.ts` to wrapt <App /> around Store context provider:

```Javascript
...
import { Provider } from 'react-redux';
import store from './store'

...
<Provider store = {store}>
  <App />
</Provider>, 
```

## Create the First Redux-based Feature

The first feature will be the Noema/Task - for those curious, noema is defined as 'content within an intentional experience' (Merriam-Webster).

To facilitate the creation of reducers and actions, we will use `redux-toolkit`. The redux-toolkit provides a set of opinionated toolset for efficient redux development. It abstracts away many of of the tedious boilerplates while following [official Redux development guides](https://redux.js.org/style-guide/style-guide#redux-style-guide)


### Create Task(Noema) Slice

In Redux Toolkit, the single global state is organized into ['slices'](https://redux-toolkit.js.org/usage/usage-guide#creating-slices-of-state).

Under `./src/features/noema` Create two files:
- `noema.types.ts` - This file includes the definitions of the models. In our case `Noema` and `Task` which extends `Noema`.
- `noema.slice.ts` - This file includes the actual state slice for Noema.

> I am using the directory name `feature` instead of `component` as to avoid confusion with the widely used term component to denote a React Component.

Don't get frustrated if `noema.slice.ts` doest not make sense to you yet. I will explain below.

> The explanation below assumes you have prior knowledge of React and/or Flux concept.

We first start by defining the state type: `NoemaState` which we will use to create the initial state and assign it to the slice.

The actual creation of slice is through the [`createSlice()` from the toolkit](https://redux-toolkit.js.org/usage/usage-guide#simplifying-slices-with-createslice).

An example of the usage:

```TypeScript
const tasksSlice = createSlice({
  name: 'tasks',
  initialState: Task[],
  reducers: {
    createTask(state, action: PayloadAction<Noema>action) {
      state.push(action.payload);
    },
    updateTask(state, action) {},
    deleteTask(state, action) {}
  }
});

export const { createTask } = tasksSlice.actions

export default tasksSlice.reducer;

console.log(createTask({ id: 123, title: 'My Task' }))
// Above {type : "posts/createPost", payload : {id : 123, title : "My Task"}}

```

As you can see, the slice abstracts many of the boilerplate code of defining actions and reducers with switch statements.

The tasksSlice looks nice and tidy. The actions are automatically derived from the reducer methods signature  in the `reducers` property passed to the createSlice.

> If you have worked with Redux but you are new to Redux-Toolkit, you would go "WHAT! you you violating the immutability of state!". Relax, you are seeing [Immer](https://immerjs.github.io/immer/docs/introduction) in action. The state passed by Redux-Toolkit is an Immer's draft object. It will properly create an immutable object by the framework.


If you need actions with side effects (impure functions), you can create Thunk functions that returns an async function:

```TypeScript
export const addTask = (task: Task) : AppThunk => async(dispatch: AppDispatch) => {
  // TODO: make API request to add noema
  
  dispatch (noemasSlice.actions.setNoema(task));
}
```

Notice that the function returns an async method in which the parameter includes dispatch. In general you would perform an action dispatch() at the end, which is a pure function.

Once the types and slice is completed, modify the store/rootReducers.ts to include the newly implemented reducer.

```TypeScript
...
import noemas from '../features/noema/noema.slice'

const rootReducer = combineReducers({ noemas/* add more reducers */});
```

### Creating a Simple Noema Service

To verify redux state, we will create a fake data in accessible location `./src/public/assets/data/test-data.json` and implement a simple service to fetch the json data.

The service (`./src/features/noema/noema.service.ts`) implementation is just a simple `fetch`. Later we replace `fetch` with `axios` for more robust functionality. 

```TypeScript
export const getNoemas = async () => {

  const data = await fetch('/assets/data/test-data.json');
  const responseData = await data.json();

  let result: Task[] = [];
  for (const entry of responseData.noemas) {
    if (entry.kind === 'noema') {
      result.push(entry as Noema);
    } else if (entry.kind === 'task') {
      result.push(entry as Task);
    }
  }

  return result;
}
```

Now, let's use the service to load the data on page load. Modify the `index.ts`:

```TypeScript
// Just before render(). Load test data on page load
if (process.env.NODE_ENV === 'development') {
  const noemaSlice = require('./features/noema/noema.slice')
  import('./features/noema/noema.service')
  .then(noemaService => {
    return noemaService.getNoemas();
  })
  .then(noemas => {
    for (const noema of noemas) {
      if (noema.kind === 'noema') {
        store.dispatch(noemaSlice.addNoema(noema));
      } else {
        store.dispatch(noemaSlice.addTask(noema));
      } 
    }
  })
  .catch(error => {
    console.error('Error while loading test data: %j', error);
  });
}
```

Now run the app with `ionic serve`. If you have Redux DevTools installed in your browser, you should see the state populated.


## Create React Component with Store

Now comes the fun: creating React components!

### Fist component: List
The first component will be to display the list of Tasks.

Under the directory `./src/features/noema` we will create three files:

The first file, `NoemaListItem.tsx`, includes the React component for the item in the list. There is no code related to redux or redux-toolkit in this component. It just received the entry from the React props.

The second file, `NoemaListItem.scss`, includes some styling to add colors to left border and time label, otherwise the default colors from Ionic would be plain black test over white background.

The third file, `NoemaList.tsx`, includes the React component that renders the actual list by iterating through the item and calling the `NoemaListItem` component we implemented above.

In NoemaList pulls the redux data, the state. This is possible because we provided the store context, remember, we wrapped the `<App />` with `<Provider>`.

```TypeScript
const noemas = useSelector(
  (state: RootState) => state.noemas
);
```

We need a page to host this list component. The `./src/pages/Journal.tsx` will be the page component that renders the list. In it, we will render Ionic page, header, toolbar and content. Within the `IonContent` we will place our `<NoemaList />` component.

Now we can add a router in the `App.tsx` so we can access it.
```TypeScript
<Route path="/journal" component={Journal} exact={true} />
```

Now, run the `$ ionic service` and open the page `http://localhost:8100/journal`. Beautiful, isn't it?


### Adding the Form Component to Be Able To Add New Entries

The app would be much more useful if we can actually add new Tasks right?
Let's create the Form component:

Add `NoemaForm.tsx` in `features/noema`.

This react component will receive as property entry and a function that will be called at completion:
```TypeScript
interface NoemaFormProps {
  entry?: Noema,
  onComplete: () => void
}
```

To simplify the form state management, we will use `react-hook-form` which we already added as dependency.
The meat of the component is the configuring the hook, the implementation of the submit handler function and the rendering.

The rendering is straight forward, so I will omit it and show only the form handling part.

```TypeScript
export default function NoemaForm({ entry, onComplete }: NoemaFormProps): JSX.Element {
  const dispatch = useDispatch();

  const { register, setValue, handleSubmit, errors } = useForm<Noema>({ defaultValues: entry });

  const onSubmit = handleSubmit((formData) => {
    // Copy over system attributes (those that are not exposed to the UI, since they are not editable field)
    formData.uid = entry?.uid;
    formData.highlighted = entry?.highlighted;
    formData.eventDate = entry ? entry.eventDate : (new Date()).toJSON();

    if (entry) {
      dispatch(updateNoema(formData));
    } else {
      dispatch(addNoema(formData));
    }
    onComplete();
  });
```

We need the `dispatch` so that we can execute the actions on submission 

The `useForm<Noema>()` returns a set of values and functions that is used to handle the form state.

We will keep it simple for now, validation will be added later.

The method `updateNoema()` is actually a thunk function, assuming that the function will make a request to the server asynchronously.

### Persisting with redux-persist

When the browser is refreshed, or even navigated back and forth, redux loses the state.

In order to keep the data across refresh, you will need to persist the state in localStorage or other storage that persists. There is a [video](https://egghead.io/lessons/javascript-redux-persisting-the-state-to-the-local-storage) about the concept by Dan Abramov.

Instead of building our own store saver/loader, we can use an existing library: redux-persist

To use redux-persist with redux-toolkit follow these instructions: 
https://stackoverflow.com/questions/63761763/how-to-configure-redux-persist-with-redux-toolkit

https://github.com/rt2zz/redux-persist/issues/988#issuecomment-654875104
