Configuring Okta
================

If you have not created a free dev account in Okta yet, go to https://developer.okta.com and create one.
(I am using kineart@gmail.com)

### Creating an Application

In order to connect your app to Okta, you need to register an application in Okta first.

1. Go to "Application" and click the "Add Application" button.
2. Select Single-Page App
  - In the Login Redirect URI enter: `http://localhost:8100/loginredirect` and `com.achievely.mobile://callback`
  - In the Logout Redirect URI enter: `http://localhost:8100/logoutredirect` and `com.achievely.mobile://endSession`
  
    The first uri is for web, the second is for the mobile native (e.g. capacitor)

### Adding a Social Identity Provider

This optional step allows you to use social media- Google, Facebook, etc - as the Identity Provider.

**NOTE** I am assuming you already have a project in [Google Dev Console](https://console.developers.google.com/) with OAuth2 enabled.

1. In Okta dev console, go to Users -> Social & Identity Providers
2. Click on Add Identity Provide and select "Google"
3. Give the IdP a name and copy paste the Client ID and Client Secret from Google Dev Console.
  - THe scopes should include `email, openid, profile`
4. When the Google IdP was successfully added, you will see an entry in the list. Click the dark circle icon to the left of the name
  - You will need the IdP ID for configuring your Ionic application. 

### Enabling CORS for API requests from mobile
When the app calls AuthService.loadUserInfo(), it actually make an API https://your-okta-domain.okta.com/oauth2/v1/token. Without the CORS configured, the request will fail with
```
Origin 'http://localhost' is therefore not allowed access
```

You need to add the origin. In Okta
1. Go to API > Trusted Origins
2. Click "Add Origin" button
3. In the name enter the app name
4. In the Origin URL enter `http://localhost`, which is what Android sends as Origin.
5. Check CORS and Redirect..., and press "Save"


## Configuring the Application

The application will pickup the configuration from the `.env` file.

.env file:
```shell
REACT_APP_AUTH_CLIENT_ID=your-client-id
REACT_APP_AUTH_SERVER_HOST=https://dev-xyz.okta.com
REACT_APP_AUTH_SCOPES="openid profile"
REACT_APP_AUTH_EXTRA_IDP=your-social-login-idp-in-okta
```

The `REACT_APP_AUTH_EXTRA_IDP` is needed if you want to enable social IdP in Okta.




## APPENDIX
Adding Google client
Go to https://console.developers.google.com/
1. Create a project
2. IN the left menu, select `Credentials`
3. On top menu, press '+ CRETE CREDENTIALS' and chose Oauth Client ID
