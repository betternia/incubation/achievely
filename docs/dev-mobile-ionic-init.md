Mobile Development with IONIC, React and Redux
==============================================

## Setup the project
git init
git config user.name "Young-Suk Ahn Park"
git config user.email "ys.ahnpark@gmail.com"
git remote add origin git@gitlab.com:creasoft-dev/ejemplos/ionic-react-taskman.git

## Create ionic app hybrid mobile app skeleton
ionic start --type=react
? Project name: achievely-mobile
? Starter template: blank
(... Will proceed with series of `npm` installations...)
? Create free Ionic account? No

Your Ionic app is ready! Follow these next steps:

- Go to your new project: cd ./achievely-mobile
- Run `ionic serve` within the app directory to see your app in the browser
- Run `ionic capacitor add` to add a native iOS or Android project using Capacitor
- Generate your app icon and splash screens using cordova-res --skip-config --copy
- Explore the Ionic docs for components, tutorials, and more: https://ion.link/docs

Test the empty project:
```
$ ionic serve
```

## Add native (Android) project

Modify the `capacity.config.json` with the correct appId

```shell
# Build the code (each time original source changes)
$ ionic build

# Add Android project (once)
$ ionic cap add android

# Copy the asset to the android project (after each ionic build)
$ ionic cap copy 

# Synchronize project by copying changes to android platform (When new plugins were added)
$ ionic cap sync

# Open the android studio
$ ionic cap open android

# All together:
$ ionic build && ionic cap copy && ionic cap sync
```

The last command will open the Android Studio(AS), an IDE based on IntelliJ.
From AS, select the preferred device (simulator) - a Pixel should be fine - and press the play button.
Once the device simulator shows up, press the "on" button to turn it on. Wait few about 2 minutes until the app is loaded in the screen.

## Adding dependencies to the project

We will add the following dependencies:
- `redux` - Redux
- `react-redux` - React bindings for Redux
- `redux-thunk` - The Redux's thunk middleware for executing impure functions
- `react-hook-form` - Library that facilitates form manipulation using React hook
- `luxon` - Date manipulation library
- `@reduxjs/toolkit` - Framework that simplifies redux development by abstracting boilerplates.
- `node-sass@^4` - Node sass
- The type definitions for the above dpendencies: `@types/react-redux`, `@types/luxon`

yarn add redux react-redux redux-thunk react-hook-form luxon
yarn add @reduxjs/toolkit

yarn add --dev node-sass@^4
yarn add --dev @types/react-redux  @types/luxon

## Using Sass
Rename the file `Home.css` under `./src/pages/` to `Home.scss` and modify `Home.tsx` to properly import the renamed file.

