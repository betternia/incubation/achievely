/**
 * This is a modified version accomodating Auth0 that does not return end_session_endpoint
 * in it's OIDC wellknown metadata.
 * The file modifed is in /node_modules/ionic-appauth/lib
 */

export class IonicEndSessionHandler {
  constructor(browser, utils = new BasicQueryStringUtils()) {
      this.browser = browser;
      this.utils = utils;
  }
  performEndSessionRequest(configuration, request) {
      return __awaiter(this, void 0, void 0, function* () {
          let url = this.buildRequestUrl(configuration, request);
          return this.browser.showWindow(url, request.postLogoutRedirectURI);
      });
  }
  buildRequestUrl(configuration, request) {
      let requestMap = {
          'id_token_hint': request.idTokenHint,
          'post_logout_redirect_uri': request.postLogoutRedirectURI,
          'state': request.state,
          // Specific to auth0
          'client_id': process.env.REACT_APP_AUTH_CLIENT_ID,
          'returnTo': request.postLogoutRedirectURI
      };
      let query = this.utils.stringify(requestMap);
      let baseUrl = configuration.endSessionEndpoint || process.env.REACT_APP_AUTH_SERVER_HOST + '/v2/logout';
      let url = `${baseUrl}?${query}`;
      return url;
  }
}
