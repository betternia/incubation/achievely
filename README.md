Achievely Mobile App
====================

Achievely is a Life Log application that tracks your tasks and achievements.

## Running the Web App

```
$ cd achievely-mobile 
$ && ionic serve
```
This will open the browser `http://localhost:8100/landing`

Running in docker
```
docker run --rm --name achv-nginx -p 8080:8100 achv-nginx
```

## Running test
From root:
```
$ yarn workspaces run test --watchAll=false
```
Optionally you can append `--watchAll=false` to run without the watch.

## Dependencies
The app depends on:
- Knoesia server
- Okta --> Auth0

