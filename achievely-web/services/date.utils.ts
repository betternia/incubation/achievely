import { parseISO, format } from 'date-fns';

/**
 * Serialized date
 * Nothing special, just wanted a short syntax.
 */
export function dateSer(): string {
  return (new Date()).toJSON();
}

/**
 * Return a string with simple time format
 * @param dateStr date in ISO 
 */
export function formatDateTime(dateStr: string): string {
  return format(parseISO(dateStr), 'Pp');
}

/**
 * Return a string with simple time format
 * @param dateStr date in ISO
 */
export function formatTime(dateStr: string): string {
  return format(parseISO(dateStr), 'p');
}
