import { RequestService } from './request.service';
import { Page } from './pagination';
import { Noema, Task } from './noema.types';

// TODO remove test when backend is implemented
const TEST_DATA_URL = '/assets/data/test-data.json';

const ROOT_PATH = process.env.REACT_APP_SERVICE_BASE_URL + '/noemas';

export async function fetchNoemas(): Promise<Page<Noema>> {

  // const data = await fetch(TEST_DATA_URL);
  const response = await RequestService.get<Page<Noema>>(`${ROOT_PATH}?sort=eventDate:desc`);

  let result: Noema[] = [];
  for (const entry of response.data.content) {
    if (entry.kind === 'noema') {
      result.push(entry as Noema);
    } else if (entry.kind === 'task') {
      result.push(entry as Task);
    }
  }
  response.data.content = result;

  return response.data;
}

export async function postNoema(noema: Noema) : Promise<Noema> {

  const response = await RequestService.post<Noema>(`${ROOT_PATH}`, noema);
  return response.data;
}

export async function putNoema(noema: Noema) : Promise<Noema> {

  const response = await RequestService.put<Noema>(`${ROOT_PATH}`, noema);
  return response.data;
}

export async function deleteNoema(noema: Noema) : Promise<Noema> {

  const response = await RequestService.delete<Noema>(`${ROOT_PATH}/${noema.uid}`);
  return response.data;
}
