
export interface Attachment {
  type: string; // media type: video, audio, photo
  data: string; // url location, or the data itself
}

export enum NoemaStatus {
  ACTIVE = 1,
  PUBLISHED = 2,
  DELETED = 0,
}

/**
 * Noema is a representation of a thought.
 * Noema in this context can be a journal entry, an idea, a note, 
 * a retrospect or a task 
 */
export interface Noema
{
  status?: number; // NoemaStatus;
  dateCreated?: string; // When the event was created
  dateModified?: string; // When the event was last modified

  // System attributes:
  uid?: string;
  userUid?: string;
  
  // Modifiable attributes:
  kind: string;  // journal, idea, note, retro, task, ritual (set of tasks)
  eventDate: string;   // When the actual event happened, usually same as createdDate
  title: string;   // title 
  content: string; // body
  highlighted?: boolean; // body

  mood?: number | undefined;    // 0 = awful, 1= bad, 2=neutral, 3=good, 4=awesome
  category?: string | undefined; // health, intellectual, emotional, spiritual, professional 
  activity?: string | undefined; // exercise, yoga, learning, friends, entertainment, etc.
  location?: string | undefined;
  weather?: string | undefined;
  weatherTemperature?: string | undefined;

  tags?: string[] | undefined;
  attachments?: Attachment[] | undefined;

  branches?: Noema[] | undefined; // Children noemas (e..g comments)
}

/**
 * Task is a representation of a work.
 * It can be estimated, and completed. It can also have subTasks
 */
export interface Task extends Noema
{
  priority?: number,
  levelOfEffort?: number, // 0: no effort, 1: small 2: medium 3: large
  // targetDateTime?: string,  Use eventDate
  completedDate?: string,
  blockedSinceDate?: string,
  canceledDate?: string,

  subTasks?: Task[]
}
