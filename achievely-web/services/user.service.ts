import { RequestService } from './request.service';
import { User, UserAccount } from './user.types';

import useSwr from 'swr'


const ROOT_PATH = process.env.BACKEND_BASE_URL;
const MY_ACCOUNT_PATH = ROOT_PATH + '/my/account';
const MY_USER_PATH = ROOT_PATH + '/my/user';
const REGISTER_ACCOUNT_PATH = ROOT_PATH + '/register-account';

export const useAccount = (bearerToken: string) => {
  const { data, error } = useSwr(MY_ACCOUNT_PATH);

  return { data, error };
}

/**
 * Registers the account 
 * @param idToken The ID Token in form of JWT as obtained from social login 
 */
export async function registerWithIdToken(accessToken: string, idToken: String, userUid?: string): Promise<UserAccount | undefined> {
  const config = populateBearerToken({}, accessToken);

  const payload = { idToken: idToken, userUid };
  console.log('**payload: ' + JSON.stringify(payload, null, 2));
  const response = await RequestService.post<UserAccount>(`${REGISTER_ACCOUNT_PATH}`, payload, config);
  return response.data;
}

export async function fetchUserAccount(accessToken: string): Promise<UserAccount> {
  const config = populateBearerToken({}, accessToken);

  const response = await RequestService.get<UserAccount>(`${MY_ACCOUNT_PATH}`, config);
  return response.data;
}

export async function fetchUserInfo(accessToken: string): Promise<User> {
  const config = populateBearerToken({}, accessToken);
  const response = await RequestService.get<User>(`${MY_USER_PATH}`, config);
  return response.data;
}

function populateBearerToken(config: any, accessToken: string) {
  config['headers'] = { Authorization: `Bearer ${accessToken}` };
  return config;
}
