/**
 * HTTP Request service
 */

import axios, { AxiosInstance, AxiosResponse, AxiosRequestConfig } from 'axios';

const SERVICE_BASE_URL = process.env.REACT_APP_SERVICE_BASE_URL || '';

export class RequestService {
  
  /**
   * Sets default header.
   * Primarily used for Authorization
   * @param key the header key, e.g. 'Authorization'
   * @param value the value, e.g. 'Bearer {TOKEN}'
   */
  static setDefaultHeader(key: string, value: string) {
    axios.defaults.headers.common[key] = value;
  }

  static async request<T = any>(config: AxiosRequestConfig): Promise<AxiosResponse<T>> {
    if (!config.url?.toLowerCase().startsWith('http')) {
      config.url = SERVICE_BASE_URL + config.url;
    }
    return axios.request<T>(config);
  }

  static async get<T = any>(url: string, config?: AxiosRequestConfig): Promise<AxiosResponse<T>> {
    const mergedConfig = { ...config, url: url};
    return RequestService.request<T>(mergedConfig);
  }

  static async delete<T = any>(url: string, config?: AxiosRequestConfig): Promise<AxiosResponse<T>> {
    // const mergedConfig: AxiosRequestConfig = { ...config, method: 'DELETE', url: url};
    return axios.delete<T>(url);
  }

  static async post<T = any>(url: string, data?: any, config?: AxiosRequestConfig): Promise<AxiosResponse<T>> {
    const mergedConfig: AxiosRequestConfig = { ...config, method: 'POST', url: url, data: data};
    return RequestService.request<T>(mergedConfig);
  }

  static async put<T = any>(url: string, data?: any, config?: AxiosRequestConfig): Promise<AxiosResponse<T>> {
    const mergedConfig: AxiosRequestConfig = { ...config, method: 'PUT', url: url, data: data};
    return RequestService.request<T>(mergedConfig);
  }

  static async patch<T = any>(url: string, data?: any, config?: AxiosRequestConfig): Promise<AxiosResponse<T>> {
    const mergedConfig: AxiosRequestConfig = { ...config, method: 'PATCH', url: url, data: data};
    return RequestService.request<T>(mergedConfig);
  }

}
