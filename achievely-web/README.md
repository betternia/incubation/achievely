# Achievely-web

The Achievely web client, based on Nextjs.

## Running Locally

### Starting the server

To start the server in dev mode, execute the following command.
```
$ yarn dev [-p <port>]
```

The server will listen to port the specified port, by default 3000. You should be able to access the page by opening a browser to `localhost:3000`.

> The command will use the `.env.local` file to populate the environment. If such file does not exist use refer `.env.sample`.

### Configuration

See [.env.sample](./.env.sample)

### Logging in 
Use the Auth0 authentication, as it is the one configured in the NoemaServer. Otherwise the login flow will fail. See Authentication section below.

## Authentication

The authentication is based on [next-auth](https://next-auth.js.org/).

The ID provider are configured in `pages/api/auth/[...nextauth].js`. Three providers: Google, Facebook and Auth0 have been configured, but keep in mind that the resource server, the NoemaServer, will try to verify the validity of the `accessToken`.  Currently NoemaServer is configured with **Auth0**.

