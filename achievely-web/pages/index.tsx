import Link from "next/link";
import { signOut, useSession } from "next-auth/client";

import { GetServerSideProps, } from "next";
import { getCsrfToken, getProviders } from "next-auth/client";

import Landing from "../components/Landing";
import Layout from "../components/Layout";
import Posts from "../components/Posts";

const testPostData = require("../test/noesia.data.json");


export default function Home({ loginForms }) {
  const [session, loading] = useSession();

  return (
    <>
      {!loading && !session && (
        <Landing loginForms={loginForms} />
      )}
      {!loading && session && (
        <Layout title="Hello World">
            <Posts data={testPostData} />
        </Layout>
      )}
    </>
  );
}


export const getServerSideProps: GetServerSideProps = async (context) => {
  const csrfToken = await getCsrfToken(context)
  const providers = await getProviders()
  // console.log('providers: ' + JSON.stringify(providers, null, 2));
  return {
    props: { 
      loginForms: {
        providers,
        csrfToken,
      }
    }
  }
}
