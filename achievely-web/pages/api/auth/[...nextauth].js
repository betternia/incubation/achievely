import NextAuth from "next-auth";
import Providers from "next-auth/providers";

import * as UserService from "../../../services/user.service"

const options = {
  providers: [
    // Providers.Google({
    //   clientId: process.env.GOOGLE_CLIENT_ID,
    //   clientSecret: process.env.GOOGLE_CLIENT_SECRET,
    // }),
    // Providers.Facebook({
    //   clientId: process.env.FACEBOOK_CLIENT_ID,
    //   clientSecret: process.env.FACEBOOK_CLIENT_SECRET,
    // }),
    Providers.Auth0({
      clientId: process.env.AUTH0_CLIENT_ID,
      clientSecret: process.env.AUTH0_CLIENT_SECRET,
      domain: process.env.AUTH0_DOMAIN,
      authorizationUrl: `https://${process.env.AUTH0_DOMAIN}/authorize?${new URLSearchParams({
        response_type: "code",
        audience: process.env.AUTH0_AUDIENCE  
      })}`,
    }),
  ],
  callbacks: {
    /**
     * @param  {object}  token     Decrypted JSON Web Token
     * @param  {object}  user      User object      (only available on sign in)
     * @param  {object}  account   Provider account (only available on sign in)
     * @param  {object}  profile   Provider profile (only available on sign in)
     * @param  {boolean} isNewUser True if new user (only available on sign in)
     * @return {object}            JSON Web Token that will be saved
     */
    async jwt(token, user, account, profile, isNewUser) {
      // Add access_token to the token right after signin
      // When account is available, it means on sign in flow, appropriate time to register new account if applicable.
      // Call Knoesia's /register-account with payload and Bearer token from account.access_token

      // TODO: Remove me
      // console.log('user'+ JSON.stringify(user, null, 2));
      // console.log('account'+ JSON.stringify(account, null, 2));
      // console.log('profile'+ JSON.stringify(profile, null, 2));
      // console.log('isNewUser'+ JSON.stringify(isNewUser, null, 2));

      if (account?.accessToken) {
        console.log('accessToken: ' + account.accessToken);
        token.accessToken = account.accessToken;
        const userAccount = await UserService.registerWithIdToken(account.accessToken, account.id_token);
        token.userUid = (await userAccount).user.uid;
      }
      console.log('token: ' + JSON.stringify(token, null, 2));

      return token;
    }
  }
  
};

export default(req, res) => NextAuth(req, res, options);
