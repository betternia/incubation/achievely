HOW-TO: Auth in Nextjs
======================

The goal is to authenticate using social network. The requirement is to integrated auth with Google, Facebook and LinedIn, optionally Twitter and Kakao.

Social auth uses OIDC/OAuth flow. There are two design considerations. First the authentication grant types: the two most popular being PKCE and Authorization Code. The second consideration is direct or delegated authentication

## Authorization grant types
The difference between PKCE and Authorization Code is that the former does the code negotiation in the client side, whereas the latter the code is obtained in the service side. In the Authorization Code, the server, upon receiving the auth code in the query string, together with app_key, it exchanges for an access token. 
Singe Page Application is recommended to use PKCE, whereas if you rely on server app, the code grant is still more secure.

For further detail in the different grant types please read the [oauth.net doc](https://oauth.net/2/grant-types/).

## Direct or Delegated

You can consider two different approaches for the social auth integration - direct or delegated.

1. The direct integration connection social network directly using framework such as Passportjs.
  
  Pros:

    - Full control of the connection
    - No additional indirection
  
  Cons:

    - Must keep up with the possible changes
    - Must handle all the credentials 

2. Using a SaaS Id and Access Management(IDAM) service such as Okta or Auth0 (which got acquired by Okta) and have it delegate to the actual social auth provider such as Facebook.

  Pros:

    - Multiple client connections are managed by the SaaS. Changes in the connection is abstracted.
    - User data is normalized, you do not need to implement adapters.
    - Added features (depending on the provider): Multi-factor Auth, Analytics and Reporting, etc.

  Cons:

    - The login link page may be constrained to the one provided by the SaaS

## Possible solutions

### Auth0 - Indirect, Code Grant 
Auth0 provides an SDK for Nextjs aptly called ["Auth0 Next.js SDK"](https://github.com/auth0/nextjs-auth0).

There are [three](https://auth0.com/docs/quickstart/webapp/nextjs/01-login) [blog](https://auth0.com/blog/introducing-the-auth0-next-js-sdk/) [entries](https://auth0.com/blog/ultimate-guide-nextjs-authentication-auth0/)


### NextAuth.js - Direct, Code Grant 
There is a popular open source library called [NextAuth.js](https://next-auth.js.org/tutorials). It supports many auth providers, Kakao, Line is included in the list.

You can actually use NextAuth.js to connect with Auth0 and Okta


Authorization callback:
http://localhost:3000/api/auth/callback/{provider}


#### Extending NextAuth

Purpose: To be able to retrieve user information from my own service and populate JWT with 

#### Auth0 returning Opaque accessToken (instead of JWT)
Auth0 by default will return an opaque accessToken, ie. not JWT. To obtain JWT token you need to include the target audience when making request to `/authorize` endpoint.  

In NextAuth, the file `provides/auth.js` had to be modified as follow
```javascript
...
const extraAuthParam = options.audience ? `&audience=${options.audience}`: '';
...
authorizationUrl: `https://${options.domain}/authorize?response_type=code${extraAuthParam}`,
```


**Sample objects:**

- callbacks.jwt() from Google
token: {
  "name": "Young Suk Ahn",
  "email": "kineart@gmail.com",
  "picture": "https://lh3.googleusercontent.com/a-/AOh14GgxLWXCQ-zQXdGAr4OlVqISkz0UGhl4m2GV7geELQ=s96-c",
  "sub": "112477426679212806404"
}

user: {
  "id": "112477426679212806404",
  "name": "Young Suk Ahn",
  "email": "kineart@gmail.com",
  "image": "https://lh3.googleusercontent.com/a-/AOh14GgxLWXCQ-zQXdGAr4OlVqISkz0UGhl4m2GV7geELQ=s96-c"
}

account: {
  "provider": "google",
  "type": "oauth",
  "id": "112477426679212806404",
  "accessToken": "ya29.a0AfH6SMA_Vz4CBmOYfrqOLbe7Psvv2-fXu7dqCCVZn_cARIqDzfdRykyMox9hVZ0LxWzKsOmiOuj9bQVSqT7lowzp5DnYWbWAHcHSTyYt2TIB_cn3M9Kk0gsH7v1jQKnZRtGZhRnRr5EBZyTDIA-fjin8dhiE",
  "accessTokenExpires": null,
  "idToken": "eyJhbGciOiJSUzI1NiIsImtpZCI6IjZhMWQyNmQ5OTJiZTdhNGI2ODliZGNlMTkxMWY0ZTlhZGM3NWQ5ZjEiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJhY2NvdW50cy5nb29nbGUuY29tIiwiYXpwIjoiOTkyNTQxMTg0MzA3LW9pdWtsNGhvaXZjbzVzNzEzaWZlb2I0cmxybHNqZjAwLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwiYXVkIjoiOTkyNTQxMTg0MzA3LW9pdWtsNGhvaXZjbzVzNzEzaWZlb2I0cmxybHNqZjAwLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwic3ViIjoiMTEyNDc3NDI2Njc5MjEyODA2NDA0IiwiZW1haWwiOiJraW5lYXJ0QGdtYWlsLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJhdF9oYXNoIjoiOFJkOXpnVlBBNXV2Q2hmN0hTXzlPdyIsImlhdCI6MTYyMzE4OTM0OCwiZXhwIjoxNjIzMTkyOTQ4fQ.KFSjOBnNBaeknyAmspUAMKZHcoxFuqAyZdqyuO5iFIHd2AtG_kjWownq0n_CulNAYQkbIBflOcpGuByt93OSpRiC4uixXm2jdyLDQqLOFXUmsD87Qh9bq3uk5gQeXO3UpwwUIgNGSaEe5_lTZYGTS7FYOUuNc6ggOxIo6VAwNYrgMA-4YIDjA0pYE7bPxn22JzV-v6WAEREwA5wMfrf2va4EPIaT_p71l8iCmxmAEG7qxlfO_xBtBuhAiIo9m_Qd9-mswPNs8G71mPuk9msxm4w9JNAKAWMOtVJjmccx42VKQoc3qmRuUcOGw4YyIa-L7Np_v9eLc1lrbu89q0-jlQ",
  "access_token": "ya29.a0AfH6SMA_Vz4CBmOYfrqOLbe7Psvv2-fXu7dqCCVZn_cARIqDzfdRykyMox9hVZ0LxWzKsOmiOuj9bQVSqT7lowzp5DnYWbWAHcHSTyYt2TIB_cn3M9Kk0gsH7v1jQKnZRtGZhRnRr5EBZyTDIA-fjin8dhiE",
  "expires_in": 3599,
  "scope": "https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile openid",
  "token_type": "Bearer",
  "id_token": "eyJhbGciOiJSUzI1NiIsImtpZCI6IjZhMWQyNmQ5OTJiZTdhNGI2ODliZGNlMTkxMWY0ZTlhZGM3NWQ5ZjEiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJhY2NvdW50cy5nb29nbGUuY29tIiwiYXpwIjoiOTkyNTQxMTg0MzA3LW9pdWtsNGhvaXZjbzVzNzEzaWZlb2I0cmxybHNqZjAwLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwiYXVkIjoiOTkyNTQxMTg0MzA3LW9pdWtsNGhvaXZjbzVzNzEzaWZlb2I0cmxybHNqZjAwLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwic3ViIjoiMTEyNDc3NDI2Njc5MjEyODA2NDA0IiwiZW1haWwiOiJraW5lYXJ0QGdtYWlsLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJhdF9oYXNoIjoiOFJkOXpnVlBBNXV2Q2hmN0hTXzlPdyIsImlhdCI6MTYyMzE4OTM0OCwiZXhwIjoxNjIzMTkyOTQ4fQ.KFSjOBnNBaeknyAmspUAMKZHcoxFuqAyZdqyuO5iFIHd2AtG_kjWownq0n_CulNAYQkbIBflOcpGuByt93OSpRiC4uixXm2jdyLDQqLOFXUmsD87Qh9bq3uk5gQeXO3UpwwUIgNGSaEe5_lTZYGTS7FYOUuNc6ggOxIo6VAwNYrgMA-4YIDjA0pYE7bPxn22JzV-v6WAEREwA5wMfrf2va4EPIaT_p71l8iCmxmAEG7qxlfO_xBtBuhAiIo9m_Qd9-mswPNs8G71mPuk9msxm4w9JNAKAWMOtVJjmccx42VKQoc3qmRuUcOGw4YyIa-L7Np_v9eLc1lrbu89q0-jlQ"
}

profile: {
  "id": "112477426679212806404",
  "email": "kineart@gmail.com",
  "verified_email": true,
  "name": "Young Suk Ahn",
  "given_name": "Young Suk",
  "family_name": "Ahn",
  "picture": "https://lh3.googleusercontent.com/a-/AOh14GgxLWXCQ-zQXdGAr4OlVqISkz0UGhl4m2GV7geELQ=s96-c",
  "locale": "en"
}

- callbacks.jwt() from Auth0+Google

token: {
  "name": "kineart",
  "email": "kineart@gmail.com",
  "picture": "https://lh3.googleusercontent.com/a-/AOh14GgxLWXCQ-zQXdGAr4OlVqISkz0UGhl4m2GV7geELQ=s96-c",
  "sub": "google-oauth2|112477426679212806404"
}
user: {
  "id": "google-oauth2|112477426679212806404",
  "name": "kineart",
  "email": "kineart@gmail.com",
  "image": "https://lh3.googleusercontent.com/a-/AOh14GgxLWXCQ-zQXdGAr4OlVqISkz0UGhl4m2GV7geELQ=s96-c"
}
account: {
  "provider": "auth0",
  "type": "oauth",
  "id": "google-oauth2|112477426679212806404",
  "accessToken": "dx1TtgyU21Rd3Nz1pKxtCdgd4eMEe9WB",
  "accessTokenExpires": null,
  "idToken": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6ImVDcDVudHdqeWRZZUVCQUQ4SlNzcSJ9.eyJnaXZlbl9uYW1lIjoiWW91bmcgU3VrIiwiZmFtaWx5X25hbWUiOiJBaG4iLCJuaWNrbmFtZSI6ImtpbmVhcnQiLCJuYW1lIjoiWW91bmcgU3VrIEFobiIsInBpY3R1cmUiOiJodHRwczovL2xoMy5nb29nbGV1c2VyY29udGVudC5jb20vYS0vQU9oMTRHZ3hMV1hDUS16UVhkR0FyNE9sVnFJU2t6MFVHaGw0bTJHVjdnZUVMUT1zOTYtYyIsImxvY2FsZSI6ImVuIiwidXBkYXRlZF9hdCI6IjIwMjEtMDYtMDhUMjI6Mzg6MTYuMDAyWiIsImVtYWlsIjoia2luZWFydEBnbWFpbC5jb20iLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiaXNzIjoiaHR0cHM6Ly9raW5lLWRldi51cy5hdXRoMC5jb20vIiwic3ViIjoiZ29vZ2xlLW9hdXRoMnwxMTI0Nzc0MjY2NzkyMTI4MDY0MDQiLCJhdWQiOiJTNXdROVZYUjA4WkF5YlN6WTJvdzZEbnFibUwxUkhFWiIsImlhdCI6MTYyMzE5MjUxMiwiZXhwIjoxNjIzMjI4NTEyfQ.jRWbUetyAf3qupRXCug76gofHQ9yLakp1fEDM5pUEoVK_vfURgOXw8cyMP2GfZ8TQh0m3BJzVrW8-GkhxrJOcNaXHJbb50-0naS-uVhtqIOQdBqNOY0U-Kyc8HUIHyTuVkmKBGIx0TZ5FwOTVaWwb5gcutsvUu8gmNhp_Fdjd68TJ_Sqqx6dMrR1ghFHG3epNKPkS4c9uj5zuT70tp3VJ4wSMiU_sGsIzHCxdfa6ghyG8wB7ufUwOJDACr_hbJ7Py2wLwoYRq_BWpDM8vGCZ6xajY11u9X6Q-hgTXjwf8tR4Jq9o8F9fl82Bgyv1BVFhqrjnNrSglSZvdf6JwscqpA",
  "access_token": "dx1TtgyU21Rd3Nz1pKxtCdgd4eMEe9WB",
  "id_token": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6ImVDcDVudHdqeWRZZUVCQUQ4SlNzcSJ9.eyJnaXZlbl9uYW1lIjoiWW91bmcgU3VrIiwiZmFtaWx5X25hbWUiOiJBaG4iLCJuaWNrbmFtZSI6ImtpbmVhcnQiLCJuYW1lIjoiWW91bmcgU3VrIEFobiIsInBpY3R1cmUiOiJodHRwczovL2xoMy5nb29nbGV1c2VyY29udGVudC5jb20vYS0vQU9oMTRHZ3hMV1hDUS16UVhkR0FyNE9sVnFJU2t6MFVHaGw0bTJHVjdnZUVMUT1zOTYtYyIsImxvY2FsZSI6ImVuIiwidXBkYXRlZF9hdCI6IjIwMjEtMDYtMDhUMjI6Mzg6MTYuMDAyWiIsImVtYWlsIjoia2luZWFydEBnbWFpbC5jb20iLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiaXNzIjoiaHR0cHM6Ly9raW5lLWRldi51cy5hdXRoMC5jb20vIiwic3ViIjoiZ29vZ2xlLW9hdXRoMnwxMTI0Nzc0MjY2NzkyMTI4MDY0MDQiLCJhdWQiOiJTNXdROVZYUjA4WkF5YlN6WTJvdzZEbnFibUwxUkhFWiIsImlhdCI6MTYyMzE5MjUxMiwiZXhwIjoxNjIzMjI4NTEyfQ.jRWbUetyAf3qupRXCug76gofHQ9yLakp1fEDM5pUEoVK_vfURgOXw8cyMP2GfZ8TQh0m3BJzVrW8-GkhxrJOcNaXHJbb50-0naS-uVhtqIOQdBqNOY0U-Kyc8HUIHyTuVkmKBGIx0TZ5FwOTVaWwb5gcutsvUu8gmNhp_Fdjd68TJ_Sqqx6dMrR1ghFHG3epNKPkS4c9uj5zuT70tp3VJ4wSMiU_sGsIzHCxdfa6ghyG8wB7ufUwOJDACr_hbJ7Py2wLwoYRq_BWpDM8vGCZ6xajY11u9X6Q-hgTXjwf8tR4Jq9o8F9fl82Bgyv1BVFhqrjnNrSglSZvdf6JwscqpA",
  "scope": "openid profile email",
  "expires_in": 86400,
  "token_type": "Bearer"
}
profile: {
  "sub": "google-oauth2|112477426679212806404",
  "given_name": "Young Suk",
  "family_name": "Ahn",
  "nickname": "kineart",
  "name": "Young Suk Ahn",
  "picture": "https://lh3.googleusercontent.com/a-/AOh14GgxLWXCQ-zQXdGAr4OlVqISkz0UGhl4m2GV7geELQ=s96-c",
  "locale": "en",
  "updated_at": "2021-06-08T22:38:16.002Z",
  "email": "kineart@gmail.com",
  "email_verified": true
}

**JWT token**
  {
    "name": "kineart",
    "email": "kineart@gmail.com",
    "picture": "https://lh3.googleusercontent.com/a-/AOh14GgxLWXCQ-zQXdGAr4OlVqISkz0UGhl4m2GV7geELQ=s96-c",
    "sub": "google-oauth2|112477426679212806404",
    "accessToken": "NiV3V63tIf2WNOJn_Z3QoknFHy0tDkW8",
    "iat": 1623200232,
    "exp": 1625792232
  }
