NextJS
======

## Installing


### Using CLI with tailwind configuration

To setup a project with Tailwind, Follow the instruction in [Tailwind documentation](https://tailwindcss.com/docs/guides/nextjs)

```
npx create-next-app -e with-tailwindcss my-project
cd my-project
```

Note `yarn create next-app` failed with nunjuck version mismatch error.

To enable TypeScript
```
touch tsconfig.json
yarn dev # this will ask you to install further dependencies. Then when run again
```
The second tie yarn dev is run, it will 1) populate the tsconfig.json file for you, 2) create `next-env.d.ts` file,


### Manual installation
```
yarn workspace achievely-web add axios date-fns jwt-decode react react-dom next next-auth swr @headlessui/react @heroicons/react

yarn workspace achievely-web add --dev @types/axios @types/react autoprefixer postcss tailwindcss typescript
```

## Logging
https://github.com/pinojs/pino-nextjs-example
