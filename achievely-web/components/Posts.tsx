import React, { Fragment } from "react";
import { Menu, Transition } from '@headlessui/react'
import { DotsHorizontalIcon } from '@heroicons/react/outline'

import { format } from 'date-fns';

const contextMenu = [
  { name: 'Edit', href: '#', current: true },
  { name: 'Share Post', href: '#', current: true },
  { name: 'Add Bookmark', href: '#', current: false },
  { name: 'Star', href: '#', current: false },
  { name: 'Comment', href: '#', current: false },
]

function classNames(...classes) {
  return classes.filter(Boolean).join(' ')
}

type ProfilePaneProps = {
  data: any
}

const Posts = ({ data }: ProfilePaneProps) => {
  
  const today = new Date();

  return (
    <div className="space-y-2">
      {/* Post form */}
      <div className="rounded-md shadow bg-gray-100 p-4 border-1 flex items-center">
        <span className="text-sm text-blue-500 break-normal">{ format(today, 'LLL d')}</span>
        <input className="rounded-lg bg-white w-full border-1 focus:outline-none" placeholder="Post a Journal or a Task"></input>
      </div>

      {/* Posts */}
      <div className="rounded-md bg-gray-200 p-2" >
        {/* Author */}
        <div className="flex items-center ">
          <img className="h-8 w-8 rounded-full " alt="Profile pic"
            src="https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80"
          />
          <div className="p-2 w-full ">
            <h2 className="text-sm font-bold">Tornado Aykay</h2>
            <span className="text-sm text-blue-500">Wed 23</span>
          </div>

        </div>

        {/* Content */}
        <div className="mt-2 space-y-2">
          {data && data.content.map((item) => {
            return (
              <div className="flex rounded-md bg-gray-50 p-2 items-start">
                <div className="w-full">{item.content}</div>

                {/* Right context button */}
                <div className="float-right ">
                  <Menu as="div" className="ml-3 relative">
                    {({ open }) => (
                      <>
                        <div>
                          <Menu.Button className="flex text-sm rounded-full focus:outline-none ">
                            <span className="sr-only">Open user menu</span>
                            <DotsHorizontalIcon className="h-5" aria-hidden="true" />
                          </Menu.Button>
                        </div>
                        <Transition
                          show={open}
                          as={Fragment}
                          enter="transition ease-out duration-100"
                          enterFrom="transform opacity-0 scale-95"
                          enterTo="transform opacity-100 scale-100"
                          leave="transition ease-in duration-75"
                          leaveFrom="transform opacity-100 scale-100"
                          leaveTo="transform opacity-0 scale-95"
                        >
                          {/* Account Menu */}
                          <Menu.Items
                            static
                            className="origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg py-1 bg-white ring-1 ring-black ring-opacity-5 focus:outline-none z-10"
                          >
                            {contextMenu.map((menuItem) => (
                              <Menu.Item key={menuItem.name}>
                                {({ active }) => (
                                  <a href="#"
                                    className={classNames(
                                      active ? 'bg-gray-100' : '',
                                      'block px-4 py-2 text-sm text-gray-700'
                                    )}
                                  >
                                    {menuItem.name}
                                  </a>
                                )}
                              </Menu.Item>
                            ))}

                          </Menu.Items>
                        </Transition>
                      </>
                    )}
                  </Menu>
                </div>
              </div>
            )
          })}
        </div>
      </div>
    </div>
  );
}

export default Posts;
