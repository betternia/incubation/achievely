/* This example requires Tailwind CSS v2.0+ */
import { Fragment } from 'react'
import { signOut, useSession } from "next-auth/client";

import { Disclosure, Menu, Transition } from '@headlessui/react'
import { HomeIcon, PlusCircleIcon, CalendarIcon, DotsCircleHorizontalIcon, BellIcon, MenuIcon, XIcon } from '@heroicons/react/outline'

const navigation = [
  { name: 'Goals', href: '#', current: true },
  { name: 'Achievements', href: '#', current: false },
  { name: 'Values', href: '#', current: false },
  { name: 'Stats', href: '#', current: false },
]

const toolbar = [
  { name: 'Home', href: '#', iconClass: HomeIcon },
  { name: 'Post', href: '#', iconClass: PlusCircleIcon },
  { name: 'Calendar', href: '#', iconClass: CalendarIcon },
]

const accountMenu = [
  { name: 'Profile', href: '#', current: true },
  { name: 'Settings', href: '#', current: false },
  { name: 'Log Out', href: '#', onClick: signOut, current: false },
]


function classNames(...classes) {
  return classes.filter(Boolean).join(' ')
}

export default function Navbar() {
  const [session, loading] = useSession();

  return (
    <Disclosure as="nav" className="bg-gray-800 fixed h-12 top-0 inset-x-0 ">
      {({ open }) => (
        <>
          <div className="max-w-7xl mx-auto px-2 sm:px-6 lg:px-8">
            <div className="relative flex items-center justify-between h-12">

              <div className="absolute inset-y-0 left-0 flex items-center sm:hidden">
                {/* Mobile menu button*/}
                <Disclosure.Button className="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-white hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white">
                  <span className="sr-only">Open main menu</span>
                  {open ? (
                    <XIcon className="block h-6 w-6" aria-hidden="true" />
                  ) : (
                    <MenuIcon className="block h-6 w-6" aria-hidden="true" />
                  )}
                </Disclosure.Button>
              </div>

              {/* Logo */}
              <div className="flex-1 flex items-center justify-center sm:items-stretch sm:justify-start">
                <div className="flex-shrink-0 flex items-center">
                  <img
                    className="block lg:hidden h-8 w-auto"
                    src="/images/achievely-logo.png"
                    alt="Workflow"
                  />
                  <img
                    className="hidden lg:block h-8 w-auto"
                    src="/images/achievely-logo.png"
                    alt="Workflow"
                  />
                </div>

                {/* Search */}

                <div className="p-8">
                  <div className="bg-white flex items-center rounded-full shadow-xl">
                    <input className="rounded-full w-full py-2 px-6 text-gray-700 leading-tight focus:outline-none" id="search" type="text" placeholder="Search" />
                  </div>
                </div>

                {/* Menu Items */}
                {/* <div className="hidden sm:block sm:ml-6">
                  <div className="flex space-x-4">
                    {navigation.map((item) => (
                      <a
                        key={item.name}
                        href={item.href}
                        className={classNames(
                          item.current ? 'bg-gray-900 text-white' : 'text-gray-300 hover:bg-gray-700 hover:text-white',
                          'px-3 py-2 rounded-md text-sm font-medium'
                        )}
                        aria-current={item.current ? 'page' : undefined}
                      >
                        {item.name}
                      </a>
                    ))}
                  </div>
                </div> */}
              </div>

              {/* Account */}
              <div className="absolute inset-y-0 right-0 flex items-center pr-2 sm:static sm:inset-auto sm:ml-6 sm:pr-0">
                <button className="bg-gray-800 p-1 rounded-full text-gray-400 hover:text-white focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white">
                  <span className="sr-only">View notifications</span>
                  <BellIcon className="h-6 w-6" aria-hidden="true" />
                </button>

                {/* Profile dropdown */}
                <Menu as="div" className="ml-3 relative">
                  {({ open }) => (
                    <>
                      <div>
                        <Menu.Button className="bg-gray-800 flex text-sm rounded-full focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white">
                          <span className="sr-only">Open user menu</span>
                          <img
                            className="h-8 w-8 rounded-full"
                            src={session && session.user.image}
                            alt=""
                          />
                        </Menu.Button>
                      </div>
                      <Transition
                        show={open}
                        as={Fragment}
                        enter="transition ease-out duration-100"
                        enterFrom="transform opacity-0 scale-95"
                        enterTo="transform opacity-100 scale-100"
                        leave="transition ease-in duration-75"
                        leaveFrom="transform opacity-100 scale-100"
                        leaveTo="transform opacity-0 scale-95"
                      >
                        {/* Account Menu */}
                        <Menu.Items
                          static
                          className="origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg py-1 bg-white ring-1 ring-black ring-opacity-5 focus:outline-none"
                        >
                          {accountMenu.map((menuItem) => (
                            <Menu.Item key={menuItem.name}>
                              {({ active }) => (
                                <a href="#"
                                  onClick = {() => menuItem.onClick && menuItem.onClick()}
                                  className={classNames(
                                    active ? 'bg-gray-100' : '',
                                    'block px-4 py-2 text-sm text-gray-700'
                                  )}
                                >{menuItem.name}</a>
                              )}
                            </Menu.Item>
                          ))}

                        </Menu.Items>
                      </Transition>
                    </>
                  )}
                </Menu>
              </div>
            </div>
          </div>

          {/* Toolbar */}
          <div className="h-16 bg-gray-200 flex justify-around">
            {toolbar.map((item) => {
              const IconClass = item.iconClass;
              return (
                <a className="w-20 text-center flex flex-col justify-center" key={item.name}
                  href={item.href}
                >
                  <div className="m-auto"><IconClass className="h-8" aria-hidden="true" /></div>
                  <div className="block">{item.name}</div>
                </a>
              )
            })}
            <Disclosure.Button className="sm:hidden inline-flex items-center justify-center p-2  hover:text-white hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-inset">
              <span className="sr-only">Open main menu</span>
              {open ? (
                <XIcon className="block h-6 w-6" aria-hidden="true" />
              ) : (
                <MenuIcon className="block h-6 w-6" aria-hidden="true" />
              )}
            </Disclosure.Button>
          </div>

          {/* Disclosure Menu */}
          <Disclosure.Panel className="sm:hidden">
            <div className="px-2 pt-2 pb-3 space-y-1 bg-gray-100">
              {navigation.map((item) => (
                <a
                  key={item.name}
                  href={item.href}
                  className={classNames(
                    item.current ? 'bg-gray-900 text-white' : 'text-gray-300 hover:bg-gray-700 hover:text-white',
                    'block px-3 py-2 rounded-md text-base font-medium'
                  )}
                  aria-current={item.current ? 'page' : undefined}
                >
                  {item.name}
                </a>
              ))}
            </div>
          </Disclosure.Panel>
        </>
      )}
    </Disclosure>
  )
}
