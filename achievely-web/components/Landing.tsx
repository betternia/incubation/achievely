import React from "react";

import { signIn } from "next-auth/client";

import Head from 'next/head'

type LandingProps = {
  loginForms: any
};

const Landing = ({ loginForms }: LandingProps) => {
  const heroStyle = {
      background: "url(/images/landing/pexels-josh-willink-670625.jpg) no-repeat center center",
      backgroundSize: "cover"
  }

  return (
    <div className="">
      <Head>
        <title>Achievely</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      
      {/* Branding */}
      <div className="relative h-96 overflow-hidden bg-indigo-500">
        <div className="absolute z-30 flex w-full ">
          <div className="relative z-30 px-10 py-8 text-white ">

            <div className="flex items-center">
              <img
                className="h-16 w-auto"
                src="/images/achievely-logo.png"
                alt="Logo"
              /> 
              <span className="px-2 text-5xl drop-shadow-md">Achievely!</span>
            </div>
            <div className="px-16 py-8">
              <span className="relative ">Recognize wins, celebrate failures, and achieve more!
            </span>
          </div>
          </div>
        </div>

        {/* Login box */}
        <div className="absolute z-30 flex right-0 px-6 py-8 ">
          <div className="relative z-30 px-6 py-8 w-80 rounded-lg bg-gray-50 text-center">
            <span className="text-lg text-blue-800">Start tracking your achievements. <br/>It’s absolutely worth it!</span>

            {/* <button onClick={signIn()}>Signin</button> */}
            <div className="space-y-2" >
            { Object.values(loginForms.providers).map( (provider: any) => {
              return (
                <div key={provider.name}>
                  {/* <form action={provider.signinUrl} method="POST">
                    <input type="hidden" name="csrfToken" defaultValue={loginForms.csrfToken} />
                    <input type="hidden" name="callbackUrl" value={provider.callbackUrl }/>
                    <button type="submit" className="w-full bg-blue-800 p-2 rounded-md text-white">Sign in with {provider.name}</button>
                  </form> */}
                  <button onClick={() => signIn(provider.id)}>Sign in with {provider.name}</button>
                </div>
              )
            }) }
            </div>

          </div>
        </div>

        <div className="absolute top-0 left-0 block w-full h-full">
          <img
            alt="Background"
            className="object-cover min-w-full h-full"
            src="/images/landing/pexels-josh-willink-670625.jpg"
          />
        </div>
      </div>

      {/* <div className="h-52 p-3 mx-auto flex flex-wrap flex-col ">
        <div className="relative z-10 col-start-1 row-start-1 px-4 pt-40 pb-3 bg-gradient-to-t from-black sm:bg-none">
          <p className="text-sm font-medium sm:mb-1 sm:text-gray-500">Entire house</p>
          <h2 className="text-xl font-semibold sm:text-2xl sm:leading-7 sm:text-black md:text-3xl">Beach House in Collingwood</h2>
        </div>
        <div>
          <img
            className="mt-10 h-16 w-auto"
            src="/images/achievely-logo.png"
            alt="Logo"
          />
          Achievely
        </div>
        <div id="hero-visual" className="w-full md:w-3/5 py-6 text-center">
          <span className="text-lg text-white">
            <img className="w-2 md:w-4/5 z-50" src="/images/logo.png" />
          </span>
        </div>
      </div> */}

    </div>
  );
}


export default Landing;
