import React from "react";
import Head from 'next/head'
import { useSession } from "next-auth/client";

import ProfilePane from './ProfilePane'
import NavBar from './NavBar'

type LayoutProps = {
  title: string,
  children: JSX.Element
};

const Layout = ({ title, children }: LayoutProps) => {

  const [session, loading] = useSession();
  
  return (
    <div className="py-28">
      <Head>
        <title>{title}</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <NavBar/>
      
      <div className="flex p-2">
        {/* Left bar */}
        {/* TODO: stick to top */}
        <ProfilePane />

        <main className="flex flex-col flex-1 w-full lg:w-80 ">
        {children}
        </main>
      </div>
      

      <footer className="flex items-center justify-center w-full h-24 border-t">
        <a
          className="flex items-center justify-center"
          href="#"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{' '} Betternia
        </a>
      </footer>
    </div>
  );
}

export default Layout;
