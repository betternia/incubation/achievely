import React from "react";
import { useSession } from "next-auth/client";

import { SupportIcon, ScaleIcon, LightningBoltIcon, ChartSquareBarIcon } from '@heroicons/react/outline'

// TODO: there is a duplicate in NavBar.tsx
const navigation = [
  { name: 'Goals', href: '#', current: true, iconClass: SupportIcon},
  { name: 'Achievements', href: '#', current: false, iconClass: LightningBoltIcon},
  { name: 'Values', href: '#', current: false, iconClass: ScaleIcon},
  { name: 'Stats', href: '#', current: false, iconClass: ChartSquareBarIcon},
]

type ProfilePaneProps = {
  
}

const ProfilePane = ({  }: ProfilePaneProps) => {
  const [session, loading] = useSession();
  return (
  <div className="hidden sm:block w-64 space-y-2" >
    <div className="rounded-md shadow bg-gray-100 w-56 m-auto p-4 text-center border-1">
      <img className="h-16 w-16 rounded-full m-auto" alt="Profile pic"
        src={session?.user?.image}
      />
      <a className="mt-3 text-gray-800 font-bold hover:underline">{session.user.name}</a>
      <h2 className="mt-2 text-sm text-gray-500">Let us see how foar this thingy goes cause like who cares what this says its a teeesstttt</h2>
    </div>

    {/* TODO: factor out the same styling as above */}
    <div className="rounded-md shadow bg-gray-100 w-56 m-auto p-4 border-1">
      <div className="block space-y-2">
      { navigation.map( (item) => {
        const IconClass = item.iconClass;
        return (
          <div className="flex space-x-2 items-center" key={item.name}>
            <IconClass className="h-5" aria-hidden="true" />
            <a className="uppercase" href={item.href}>{item.name}</a>
          </div>
        )
      })}
      <a className=""></a>
      </div>
    </div>

  </div>
  );
}

export default ProfilePane;
