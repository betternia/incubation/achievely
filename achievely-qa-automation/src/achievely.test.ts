import './env';
import { Page } from "playwright";
import { describe, it, expect } from "@playwright/test";

const USER_ID = process.env.USER_ID;
const USER_PWD = process.env.USER_PWD;

async function performLoginOkta(page: Page): Promise<void> {
  const loginBtn = await page.waitForSelector('ion-button[data-qa="login-btn"]');
  await loginBtn.click();

  const usernameInput = await page.waitForSelector('input[name="username"]');
  await usernameInput.type(USER_ID);
  // await usernameInput.press('Enter');

  const passwordInput = await page.waitForSelector('input[name="password"]');
  await passwordInput.type(USER_PWD);
  await passwordInput.press('Enter');
  
}

const performlogin = performLoginOkta;

describe("Achievely", () => {
  it("Login", async ({ page }) => {
    await page.goto("http://localhost:8100/");

    await performlogin(page);

    const name = await page.innerText("ion-title");
    expect(name).toBe("Daily");
  });

  it("Login", async ({ page }) => {
    await page.goto("http://localhost:8100/");

    await performlogin(page);

    const addBtn = await page.waitForSelector('ion-fab-button[data-qa="add-tab-btn"]');
    await addBtn.click();
    
    const addNoemaBtn = await page.waitForSelector('ion-fab-button[data-qa="add-noema-tab-btn"]');
    await addNoemaBtn.click();

    const titleInput = await page.waitForSelector('input[name="title"]');
    await titleInput.type('QA - Title');

    const contentInput = await page.waitForSelector('textarea[name="content"]');
    await contentInput.type('QA - Hello from QA!');

    const submitBtn = await page.waitForSelector('button[type="submit"]');
    await submitBtn.click();

    const listItems = await page.$$('ion-label[data-qa="noema-list-item"]');
    const title = await (await listItems[0].waitForSelector('h3')).innerText();

    // console.log('title: ' + title);

    expect(title).toBe('QA - Title')
  });
});
